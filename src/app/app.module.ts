import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { AuthComponent } from './layout/auth/auth.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { MenuItems } from './shared/menu-items/menu-items';
import { BreadcrumbsComponent } from './layout/admin/breadcrumbs/breadcrumbs.component';
import { AuthGuard } from './modules/guard/auth.guard';
import { LogoutComponent } from './modules/auth/logout/logout.component';
import { ConfigService } from './services/config/config-service';
import { HttpClientModule , HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectOptionService } from './shared/elements/select-option.service';
import { SimpleModalModule } from 'ngx-simple-modal';
import { AlertComponent } from 'src/app/modules/alert/alert.component';
import { UserLoginService } from 'src/app/services/user-login/user-login.service';
import { BasicAuthInterceptor } from './services/helpers/basic-auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    BreadcrumbsComponent,
    LogoutComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    NgbModule,
    SimpleModalModule.forRoot({container: "modal-container"})
  ],
  entryComponents: [
    AlertComponent
  ],
  providers: [
    AuthGuard,
    MenuItems,
    SelectOptionService,
    UserLoginService,
    NgbActiveModal,
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: BasicAuthInterceptor, 
      multi: true 
    },
    ConfigService,
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return () => {
          //Make sure to return a promise!
          return configService.loadAppConfig();
        };
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }