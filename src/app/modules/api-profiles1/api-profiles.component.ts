// import { Component, OnInit } from '@angular/core';
// import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
// import { Profile } from 'src/app/services/admin-function-services/api-profile/api-profile';
// import { ProfileService } from 'src/app/services/admin-function-services/api-profile/api-profile.service';
// import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
// import swal from 'sweetalert2';

// @Component({
//   selector: 'app-api-profiles',
//   templateUrl: './api-profiles.component.html',
//   styleUrls: ['./api-profiles.component.scss',
//               '../../../assets/icon/icofont/css/finsense-common.scss']
// })
// export class ApiProfilesComponent implements OnInit {

//   profileForm: FormGroup;
//   updateProfileForm: FormGroup;
//   profileAPIList = [];
//   profileList = true;
//   addProfile = true;
//   viewProfile = true;
//   editProfile = true;
//   viewProfileButtons = true;
//   profileApiUrlList = [];
//   profileApiUrlListTemp = [];
//   profileRowData;
//   result = [];
//   profileData = [];
//   upadateProfile = true;
//   isProfileExist:boolean = false;

//   constructor(public selectOptionService: SelectOptionService, 
//               public profileService: ProfileService, public formBuilder: FormBuilder) {
//     this.getProfileList();
//   }

//   ngOnInit() {
//     this.profileForm = this.formBuilder.group({
//       profileId: ['', Validators.required],
//       profileName: ['', Validators.required],
//       profileDescription: ['', [Validators.required]],
//       profileAPIs: ['', [Validators.required]]
//     });

//     this.updateProfileForm = this.formBuilder.group({
//       updateProfileId: ['', Validators.required],
//       updateProfileName: ['', Validators.required],
//       updateProfileDescription: ['', [Validators.required]],
//       updateProfileAPIs: ['', [Validators.required]]
//     });
//   }

//   createProfile() {

//     const profileListRequest: Profile = { 
//       profileId: this.profileForm.get('profileId').value,
//       profileName: this.profileForm.get('profileName').value,
//       profileDescription: this.profileForm.get('profileDescription').value,
//       profileApis: [this.profileApiUrlListTemp],
//       createdBy: localStorage.getItem('currentUser'),
//       updatedBy: null,
//       creationDate: new Date(),
//       updationDate: null,
//       approvalStatus: this.APPROVAL_STATUS.PENDING_APPROVAL
//     }
//     this.isProfileExist = this.checkProfileExistence(this.profileForm.get('profileId').value);
//     console.log(this.isProfileExist);

//     if(this.isProfileExist){
//       alert("Profile Id already exists");
//     } else {
//       this.createProfileResponse(profileListRequest);
//     }
//     console.log(" Profile Request: ", JSON.stringify(profileListRequest));
// }

// createProfileResponse(profileListRequest) {
//   this.profileRequestService.createProfile(profileListRequest).subscribe(data => {
//     console.log(" Profile Response: ", JSON.stringify(data));
//     this.addProfile = !this.addProfile;
//     this.profileList = !this.profileList;
//   })
// }

// checkProfileExistence(profileId: string):boolean {
//   return this.profileAPIList.some(r => r.profileId === profileId);
// }

// getProfileList() {
//   this.profileRequestService.fetchProfileList().subscribe(data => {
//     console.log("Get Profile List Response: "+ JSON.stringify(data));
//     this.profileAPIList = data['profileListRequest'];
//     this.profileApiUrlList = data['profileApiUrl'];
//   })
// }

// updateProfile() {

//   const profileListRequest: ProfileListRequest = { 
//     profileId: this.updateProfileForm.get('updateProfileId').value,
//     profileName: this.updateProfileForm.get('updateProfileName').value,
//     profileDescription: this.updateProfileForm.get('updateProfileDescription').value,
//     profileApis: this.profileApiUrlListTemp,
//     createdBy: null,
//     updatedBy: localStorage.getItem('currentUser'),
//     creationDate: null,
//     updationDate: new Date(),
//     approvalStatus: this.APPROVAL_STATUS.PENDING_APPROVAL
//   } 

//   this.profileRequestService.updateProfile(profileListRequest).subscribe(data => {
//     console.log(" Update Profile Response: ", JSON.stringify(data));
//     this.editProfile = !this.editProfile;
//     this.profileList = !this.profileList;
//   })
//   this.getProfileList();
// }

// editProfileRecord(profileId) {
//   this.profileRequestService.fetchProfile(profileId).subscribe(data => {
//     console.log("Profile Response: "+ JSON.stringify(data));
//     this.getUpdateRow(this.setValueToObject(data));
//     this.profileList = false;
//     this.addProfile = true;
//     this.viewProfile = true;
//     this.editProfile = false;
//   });
// }

// deleteProfileRecord(profileId) {
//   swal({
//     title: 'Are you sure?',
//     text: 'You want to delete record!',
//     type: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     confirmButtonText: 'Yes, delete it!'
//   }).then((result) => {
//     if (result.value) {
//       swal({
//         type: 'success',
//         title: 'Deleted!',
//         text: "Your record has been deleted.",
//         showConfirmButton: true
//       })
//       this.deleteChannel(profileId);
//       this.getProfileList();
//     }
//   });
// }

// deleteChannel(profileId) {
//   this.profileRequestService.deleteProfile(profileId).subscribe(data => {
//   console.log("Deleted Profile Response: "+ JSON.stringify(data));
//   this.profileList = true;
//   this.addProfile = true;
//   this.viewProfile = true;
//   this.editProfile = true;
// });
// }

// getUpdateRow(profileData){

//   this.profileApiUrlListTemp = profileData.profileApis;
  
//   this.updateProfileForm.setValue({
//     updateProfileId: profileData.profileId,
//     updateProfileName: profileData.profileName,
//     updateProfileDescription: profileData.profileDescription,
//     updateProfileAPIs: profileData.profileApis
//   })
// }

// viewProfileRecord(profileId) {
//   this.profileRequestService.fetchProfile(profileId).subscribe(data => {
//     console.log("Profile Response: "+ JSON.stringify(data));
//     if(localStorage.getItem('currentUser') == data['createdBy']){
//       this.viewProfileButtons = true;
//     } else {
//       this.viewProfileButtons = false;
//     }
//     this.viewProfileDetails(this.setValueToObject(data));
//     this.profileList = false;
//     this.addProfile = true;
//     this.viewProfile = false;
//   });
// }

// viewProfileDetails(profileData){
//   this.profileRowData = profileData;
// }

// setValueToObject(data){
//   const profileListRequest: ProfileListRequest = {
//     profileId: data['profileId'],
//     profileName: data['profileName'],
//     profileDescription: data['profileDescription'],
//     profileApis: data['profileApis'],	
//     createdBy: data['createdBy'],
//     updatedBy: data['updatedBy'],
//     creationDate: data['creationDate'],
//     updationDate: data['updationDate'],
//     approvalStatus: data['approvalStatus'],
//   }
//   return profileListRequest;
// }

// approveConfirm(profileData) {

//   if(profileData.createdBy == localStorage.getItem("currentUser")){
//     swal({
//       type: 'error',
//       title: 'Oops...',
//       text: 'Creator does not allow to approve!',
//     })
//   }
//   else{
//     swal({
//       title: 'Are you sure?',
//       text: 'You want to approve record!',
//       type: 'warning',
//       showCloseButton: true,
//       showCancelButton: true,
//       confirmButtonColor: '#3085d6',
//       cancelButtonColor: '#d33',
//       confirmButtonText: 'Approve',
//     }).then((result) => {
//       if (result.value) {
//         this.setValueToObject(profileData);
//         this.getProfileApproval(profileData);
//       }
//     })
//   }
// }

// rejectConfirm(profileData) {

//   if(profileData.createdBy == localStorage.getItem("currentUser")){
//     swal({
//       type: 'error',
//       title: 'Oops...',
//       text: 'Creator does not allow to reject!',
//     })
//   }
//   else{
//     swal({
//       title: 'Are you sure?',
//       text: 'You want to reject record!',
//       type: 'warning',
//       showCloseButton: true,
//       showCancelButton: true,
//       confirmButtonColor: '#3085d6',
//       cancelButtonColor: '#d33',
//       confirmButtonText: 'Reject',
//     }).then((result) => {
//       if (result.value) {
//         this.setValueToObject(profileData);
//         this.getRejectedProfile(profileData);
//       }
//     })
//   }
// }

// getProfileApproval(data){
//   const profileListRequest: ProfileListRequest = {
//     profileId: data['profileId'],
//     profileName: data['profileName'],
//     profileDescription: data['profileDescription'],
//     profileApis: data['profileApis'],	
//     createdBy: data['createdBy'],
//     updatedBy: data['updatedBy'],
//     creationDate: data['creationDate'],
//     updationDate: data['updationDate'],
//     approvalStatus: this.APPROVAL_STATUS.APPROVE
//   }
//   this.profileRequestService.updateProfile(profileListRequest).subscribe(data => {
//     console.log(" Approved Profile Response: ", JSON.stringify(data));
//   })

//   swal({
//     position: 'center',
//     type: 'success',
//     title: 'Successfully Approved!',
//     showConfirmButton: false,
//     timer: 1500
//   });
//   this.getProfileList();
// }

// getRejectedProfile(data){
//   const profileListRequest: ProfileListRequest = {
//     profileId: data['profileId'],
//     profileName: data['profileName'],
//     profileDescription: data['profileDescription'],
//     profileApis: data['profileApis'],	
//     createdBy: data['createdBy'],
//     updatedBy: data['updatedBy'],
//     creationDate: data['creationDate'],
//     updationDate: data['updationDate'],
//     approvalStatus: this.APPROVAL_STATUS.REJECT
//   }
//   this.profileRequestService.updateProfile(profileListRequest).subscribe(data => {
//     console.log(" Rejected Profile Response: ", JSON.stringify(data));
//   })

//   swal({
//     position: 'center',
//     type: 'success',
//     title: 'Successfully Rejected!',
//     showConfirmButton: false,
//     timer: 1500
//   });
//   this.getProfileList();
// }

// getselectedApiUrl() {
//   for (var item=0; item < this.profileApiUrlListTemp.length; item++) {
//     var aipUrl=this.profileApiUrlListTemp[item].apiUrl;
//     this.result.push(aipUrl);
//   }
// }

// onCheckboxChange(option: IDataOption , event:any) {
//   if(event.target.checked) {
//           this.profileApiUrlListTemp.push(option.apiUrl);
//   }
//   else {
//       for(var i=0 ; i < this.profileApiUrlListTemp.length; i++) {
//           if(this.profileApiUrlListTemp[i] == option.apiUrl){
//             this.profileApiUrlListTemp.splice(i,1);
//           }
//         }
//       }
// }

// isSelected(option){
//   return this.profileApiUrlListTemp.indexOf(option.apiUrl) >= 0;
// }

// submitForm() {
//   this.markFormTouched(this.profileForm);
//   if (this.profileForm.valid) {
//     // You will get form value if your form is valid
//     var formValues = this.profileForm.getRawValue;
//     this.createProfile();
//     this.getProfileList();
//   } else {
//     //alert("Something went wrong");
//   }
// };

// markFormTouched(group: FormGroup | FormArray) {
//   Object.keys(group.controls).forEach((key: string) => {
//     const control = group.controls[key];
//     if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
//     else { control.markAsTouched(); };
//   });
// };

// toggleProfile() {
//   this.editProfile = true;
//   this.profileList = true;
//   this.addProfile = true;
//   this.viewProfile = true;
// }

// toggleAddProfile() {
//   this.profileList = !this.profileList;
//   this.addProfile = !this.addProfile;
//   this.getProfileList();
// }

// toggleCloseViewProfileRecord(){
//   this.profileList = true;
//   this.addProfile = true;
//   this.viewProfile = true;
// }

// toggleEditProfile() {
//   this.editProfile = true;
//   this.profileList = true;
//   this.addProfile = true;
//   this.viewProfile = true;
// }

// private APPROVAL_STATUS = {
//     APPROVE: "Approve",
//     REJECT: "Reject",
//     PENDING_APPROVAL: "Pending Verification"
//   };
// }
