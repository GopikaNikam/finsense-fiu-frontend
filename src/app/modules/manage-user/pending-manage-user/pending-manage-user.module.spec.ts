import { PendingManageUserModule } from './pending-manage-user.module';

describe('PendingManageUserModule', () => {
  let pendingManageUserModule: PendingManageUserModule;

  beforeEach(() => {
    pendingManageUserModule = new PendingManageUserModule();
  });

  it('should create an instance', () => {
    expect(pendingManageUserModule).toBeTruthy();
  });
});
