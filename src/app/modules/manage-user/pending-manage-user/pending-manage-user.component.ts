import { Component, OnInit, Input } from '@angular/core';
import { ManageUserService } from 'src/app/services/manage-user/manage-user.service';
import { AlertComponent } from '../../alert/alert.component';
import { SimpleModalService } from 'ngx-simple-modal';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ManageUser, ApproveManageUser } from 'src/app/services/manage-user/manage-user';

@Component({
  selector: 'ngbd-modal-approve',
  styleUrls: ['./pending-manage-user.component.scss',
              '../../../../assets/icon/icofont/css/icofont.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to approve consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="approveUser(data)">
                    Approve
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalApprove {
 @Input() data

  constructor(public modal: NgbActiveModal, public router: Router, 
              public simpleModalService: SimpleModalService, public manageUserService: ManageUserService ) {}

    approveUser(data) {
      if(sessionStorage.getItem("authorizationToken") != null) {
        const approveManageUser : ApproveManageUser = {
          userId: data['userId']
        }
      
        this.manageUserService.approveUser(approveManageUser).subscribe(data => {
          if(data['errors']) {
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else if(data['body']) {
            this.simpleModalService.addModal(AlertComponent, {
              message: "User has been approved"
            })
          } else {
            if (typeof data["body"] == 'undefined') {
              this.simpleModalService.addModal(AlertComponent, {
                message: 'Data not found'
              })
            }
          }      
          console.log("Approved User Response: ", JSON.stringify(data));
          this.modal.close();
          this.router.navigate(['/manage-user/approve-manage-user/']);
        });
      } else {
          this.router.navigate(['/login']);
          console.log("Authorization token is null");
      }
    }  
  }

@Component({
  selector: 'ngbd-modal-reject',
  styleUrls: ['./pending-manage-user.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to reject consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="rejectUser(data)">
                    Reject
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalReject {
 @Input() data
  constructor(public modal: NgbActiveModal, public router: Router, 
              public simpleModalService: SimpleModalService, public manageUserService: ManageUserService ) {}

    rejectUser(data) {
      if(sessionStorage.getItem("authorizationToken") != null) {
          this.manageUserService.deleteUser(data['userId']).subscribe(data => {
            console.log("Deleted User Response: "+ data);
          });
          this.simpleModalService.addModal(AlertComponent, {
            message: 'User has been rejected.'
          })
          this.modal.close();
          this.router.navigate(['/manage-user/approve-manage-user/']);
      } else {
          this.router.navigate(['/login']);
          console.log("Authorization token is null");
      }
   }
}

@Component({
  selector: 'app-pending-manage-user',
  templateUrl: './pending-manage-user.component.html',
  styleUrls: ['./pending-manage-user.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class PendingManageUserComponent implements OnInit {

  public userList = true; viewUpdatedUserDetails = true; viewUser = true;
  public result: any; usersResultList: any
  public userId: string; userName: string; userRole: string;  createdBy: string; updatedBy: string;
  public creationDate: Date; updationDate: Date;
  public userDetails; updatedUserDetails;
  public usersList = [];
  
  constructor(public manageUserService: ManageUserService, public modalService: NgbModal,
              public simpleModalService: SimpleModalService, public router: Router) { 
    this.fetchUserList();
  }

  ngOnInit() {}

  getApproveModal(data: any) {
    if(data['createdBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Creator " + data['createdBy']+ " and approver " 
                + sessionStorage.getItem('currentUser') + " can not be same while approving user"        
      })
    } else if(data['updatedBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Updater " + data['updatedBy']+ " and approver " 
                + sessionStorage.getItem('currentUser') + " can not be same while approving user"        
      })
    } else {
        this.openApproveModal(data);
    }
  }

  openApproveModal(data: any) {
    const approveModalRef =  this.modalService.open(NgbdModalApprove,{ centered: true });
    approveModalRef.componentInstance.data = data;
  }

  getRejectModal(data: any) {
    if(data['createdBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Creator " + data['createdBy']+ " and approver " 
                + sessionStorage.getItem('currentUser') + " can not be same while approving user"        
      })
    } else if(data['updatedBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Updater " + data['updatedBy']+ " and approver " 
                + sessionStorage.getItem('currentUser') + " can not be same while approving user"        
      })
    } else {
      this.openRejectModal(data);
    }
  }

  openRejectModal(data: any) {
    const rejectModalRef =  this.modalService.open(NgbdModalReject,{ centered: true });
    rejectModalRef.componentInstance.data = data;
  }

  fetchUserList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.fetchUserList().subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          this.usersResultList = data['body']
          this.usersList = this.usersResultList["manageUsers"]
          console.log("User List: "+ JSON.stringify(this.usersResultList));
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  viewUserRecord(userId) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.fetchUserDetails(userId).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
          }
        } else {
          console.log("User Details Response: "+ JSON.stringify(data));
          this.result = data['body']
          this.viewUserDetails(this.setValueToObject(this.result.manageUser))
          this.userList = false;
          this.viewUpdatedUserDetails = false;
          this.fetchApprovedUserDetails(userId);
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    } 
  }

  fetchApprovedUserDetails(userId) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.fetchApprovedUserDetails(userId).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
          }
        } else {
          console.log("Approved User Details: "+ JSON.stringify(data));
          this.result = data['body']
          this.viewUserDetail(this.setValueToObject(this.result.manageUser));
          if(this.userDetails != null) {
            if(this.userDetails.userId == this.updatedUserDetails.userId) {
              this.userList = false;
              this.viewUser = false;
              this.viewUpdatedUserDetails = true;
              this.setValues(this.updatedUserDetails, this.userDetails);
            }
          }
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  setValues(updatedUserDetails, userDetails) {
    if(updatedUserDetails.userId != userDetails.userId) {
       this.userId = updatedUserDetails.userId
    }

    if(updatedUserDetails.userName != userDetails.userName) {
      this.userName = updatedUserDetails.userName
    }

    if(updatedUserDetails.userRole != userDetails.userRole) {
      this.userRole = updatedUserDetails.userRole
    }
 
    if(updatedUserDetails.createdBy != userDetails.createdBy) {
      this.createdBy = updatedUserDetails.createdBy
    }  
    
    if(updatedUserDetails.creationDate != userDetails.creationDate) {
      this.creationDate = updatedUserDetails.creationDate
    } 
    
    if(updatedUserDetails.updatedBy != userDetails.updatedBy) {
      this.updatedBy = updatedUserDetails.updatedBy
    }  
    
    if(updatedUserDetails.updationDate != userDetails.updationDate) {
      this.updationDate = updatedUserDetails.updationDate
    } 
  }

  viewUserDetails(updatedUserDetails){
    this.updatedUserDetails = updatedUserDetails;
  }

  viewUserDetail(userDetails){
    this.userDetails = userDetails;
  }

  setValueToObject(data) {
    const manageUser: ManageUser = {
      userId:           data['userId'],
      userName:         data['userName'],
      userRole:         data['userRole'],
      password:         data['password'],
      confirmPassword:  data['confirmPassword'],
      resetPassword:    data['resetPassword'],
      action:           data['action'],
      active:           data['active'],
      createdBy:        data['createdBy'],
      creationDate:     data['creationDate'],
      updatedBy:        data['updatedBy'],
      updationDate:     data['updationDate'],
      approvedBy:       data['approvedBy'],
      approvedDate:     data['approvedDate']
    }
    return manageUser;
}

  toggleCloseUserRecord() {
    this.userList = true;
    this.viewUpdatedUserDetails = true;
    this.viewUser = true;
  }
}