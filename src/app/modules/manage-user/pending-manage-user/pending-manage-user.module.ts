import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PendingManageUserComponent, NgbdModalApprove, NgbdModalReject } from './pending-manage-user.component';
import { PendingManageUserRoutingModule } from './pending-manage-user-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';

@NgModule({
  imports: [
    CommonModule,
    PendingManageUserRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  declarations: [ PendingManageUserComponent, NgbdModalApprove, NgbdModalReject ],
  providers: [ SelectOptionService ], 
  bootstrap: [ PendingManageUserComponent ],
  entryComponents: [ NgbdModalApprove, NgbdModalReject ]
})
export class PendingManageUserModule { }
