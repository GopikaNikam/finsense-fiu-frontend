import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingManageUserComponent } from './pending-manage-user.component';

describe('PendingManageUserComponent', () => {
  let component: PendingManageUserComponent;
  let fixture: ComponentFixture<PendingManageUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingManageUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingManageUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
