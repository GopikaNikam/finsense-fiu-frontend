import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingManageUserComponent } from './pending-manage-user.component';

const routes: Routes = [
  {
    path: '',
    component: PendingManageUserComponent,
    data: {
      title: 'Pending Manage User',
      icon: 'ti-anchor',
      caption: 'Pending Manage User',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PendingManageUserRoutingModule { }
