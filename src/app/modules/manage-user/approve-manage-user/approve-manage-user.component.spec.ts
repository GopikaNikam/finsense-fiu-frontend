import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveManageUserComponent } from './approve-manage-user.component';

describe('ApproveManageUserComponent', () => {
  let component: ApproveManageUserComponent;
  let fixture: ComponentFixture<ApproveManageUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveManageUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveManageUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
