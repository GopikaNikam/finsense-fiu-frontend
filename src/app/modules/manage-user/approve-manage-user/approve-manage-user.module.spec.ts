import { ApproveManageUserModule } from './approve-manage-user.module';

describe('ApproveManageUserModule', () => {
  let approveManageUserModule: ApproveManageUserModule;

  beforeEach(() => {
    approveManageUserModule = new ApproveManageUserModule();
  });

  it('should create an instance', () => {
    expect(approveManageUserModule).toBeTruthy();
  });
});
