import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApproveManageUserRoutingModule } from './approve-manage-user-routing.module';
import { ApproveManageUserComponent, NgbdModalDelete } from './approve-manage-user.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';

@NgModule({
  imports: [
    CommonModule,
    ApproveManageUserRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  declarations: [ ApproveManageUserComponent, NgbdModalDelete ],
  providers: [ SelectOptionService ], 
  bootstrap: [ ApproveManageUserComponent ],
  entryComponents: [ NgbdModalDelete ]
})
export class ApproveManageUserModule { }
