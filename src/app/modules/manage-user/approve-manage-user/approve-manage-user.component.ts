import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { ManageUserService } from 'src/app/services/manage-user/manage-user.service';
import { AlertComponent } from '../../alert/alert.component';
import { SimpleModalService } from 'ngx-simple-modal';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManageUser } from 'src/app/services/manage-user/manage-user';

@Component({
  selector: 'ngbd-modal-delete',
  styleUrls: ['./approve-manage-user.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template: 
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to delete consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="deleteApprovedUser(userId)">
                    Delete
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalDelete {
 @Input() userId;
 public result: any;

  constructor(public modal: NgbActiveModal, public router: Router, 
              public simpleModalService: SimpleModalService, 
              public manageUserService: ManageUserService ) {}

  deleteApprovedUser(userId) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.deleteApprovedUser(userId).subscribe(data => { 
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          this.simpleModalService.addModal(AlertComponent, {
            message: "User has been deleted"
          })
        }
        this.modal.close();
        this.router.navigate(['/manage-user/pending-manage-user/']);
        console.log("Deleted Approved User Response: "+ JSON.stringify(data));
      });
    }  else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }
}

@Component({
  selector: 'app-approve-manage-user',
  templateUrl: './approve-manage-user.component.html',
  styleUrls: ['./approve-manage-user.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class ApproveManageUserComponent implements OnInit {

  public updateManageUserForm : FormGroup;
  public manageUserList = true; viewManageUser = true; editManageUser = true;
  public approvedUsersList: any; approvedUserResult: any; approvedUserDetails; result: any; 
  public roles = [];

  constructor(public formBuilder: FormBuilder, public manageUserService: ManageUserService,
              public simpleModalService: SimpleModalService, public router: Router,
              public modalService: NgbModal) { 
    this.updateManageUserForm = formBuilder.group({
      updateUserId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])],
      updateUsername: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator])],
      updateUserRole: ['', Validators.required ],
      updatePassword: [''],
      updateConfirmPassword: ['']
    })
    this.fetchRoleList();
    this.fetchApprovedUserList(); 
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  ngOnInit() {}

  openDeleteModal(userId: string) {
    const deleteModalRef =  this.modalService.open(NgbdModalDelete,{ centered: true });
    deleteModalRef.componentInstance.userId = userId;
  }

  fetchRoleList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.fetchRoleList().subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
          }
        } else {
          this.result = data["body"];
          this.roles = this.result['roles'];
          console.log("Role List: "+ JSON.stringify(this.roles));
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  fetchApprovedUserList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.fetchApprovedUserList().subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          this.approvedUserResult = data['body'];
          this.approvedUsersList = this.approvedUserResult.manageUsers.filter(a => a.active == 'Y');
          console.log("Approved User List Response: "+ JSON.stringify(this.approvedUserResult));
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  fetchApprovedUserDetails(userId) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.fetchApprovedUserDetails(userId).subscribe(data => {
        this.viewUserDetails(data["body"]);
        console.log("Approved User Response: "+ JSON.stringify(data));
        this.manageUserList = false;
        this.viewManageUser = false;
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  updateApprovedUserDetails() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      let password, confirmPassword;
      if(this.updateManageUserForm.get('updatePassword').value === "" 
          || this.updateManageUserForm.get('updateConfirmPassword').value === ""){
          password = null;
          confirmPassword = null;
      }  else{
          password = this.updateManageUserForm.get('updatePassword').value;
          confirmPassword = this.updateManageUserForm.get('updateConfirmPassword').value;
      }
      const manageUser: ManageUser = {
        userId: this.updateManageUserForm.get('updateUserId').value,
        userName: this.updateManageUserForm.get('updateUsername').value,
        userRole: this.updateManageUserForm.get('updateUserRole').value,
        password: password,
        confirmPassword: confirmPassword,
        resetPassword: null,
        action: null,
        active: null,
        createdBy: null,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        approvedBy: null,
        approvedDate: null
      }

    this.manageUserService.updateApprovedUserDetails(manageUser).subscribe(data => {
      if(data['errors']) {
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
          this.simpleModalService.addModal(AlertComponent, {
            message: result["errorMsg"]
          })
        }
      } else {
          this.approvedUserDetails = data["manageUsers"]
          console.log("Updated User Response: "+ JSON.stringify(data));
          this.router.navigate(['/manage-user/pending-manage-user'])
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  editUserRecord(userId) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.fetchApprovedUserDetails(userId).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
          }
        } else {
          this.getUpdateRow(data["body"]);
          this.manageUserList = false;
          this.viewManageUser = true;
          this.editManageUser = false;
          console.log("Updated Approved User Response: "+ JSON.stringify(data));
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  getUpdateRow(userDetails) {
    this.updateManageUserForm.setValue({
      updateUserId: userDetails.manageUser.userId,
      updateUsername: userDetails.manageUser.userName,
      updateUserRole: userDetails.manageUser.userRole,
      updatePassword: this.updateManageUserForm.get('updatePassword').value,
      updateConfirmPassword: this.updateManageUserForm.get('updateConfirmPassword').value,
    })
  }

  viewUserDetails(userDetails){
    this.approvedUserDetails = userDetails.manageUser;
  }

  formValidation() {
    var updatePassword = this.updateManageUserForm.get('updatePassword').value
    var updateConfirmPassword = this.updateManageUserForm.get('updateConfirmPassword').value

    if(updatePassword != null && updateConfirmPassword != null) {
      if(this.passwordValidation(updatePassword, updateConfirmPassword)) {
          this.updateApprovedUserDetails();
      }
    }
    return false;
  }

  passwordValidation(updatePassword, updateConfirmPassword) {
    if (!(updatePassword === updateConfirmPassword)) {
      this.simpleModalService.addModal(AlertComponent, {
        message: 'Password and confirm password does not match',
      })
      return false;
    } 
    return true;
  }

  submitForm() {
    this.markFormTouched(this.updateManageUserForm);
    if (this.updateManageUserForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.updateManageUserForm.getRawValue;
      this.formValidation();
    } else {
      alert("Validation Failed");
    }
  };
  
  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
        else { control.markAsTouched(); };
    });
  };

  toggleUser() {
    this.editManageUser = true;
    this.manageUserList = true;
    this.viewManageUser = true;
  }

  toggleCloseUserRecord(){
    this.manageUserList = true;
    this.viewManageUser = true;
  }
}
