import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { ManageUserService } from 'src/app/services/manage-user/manage-user.service';
import { SimpleModalService } from 'ngx-simple-modal';
import { AlertComponent } from '../../alert/alert.component';
import { ManageUser } from 'src/app/services/manage-user/manage-user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-manage-user',
  templateUrl: './create-manage-user.component.html',
  styleUrls: ['./create-manage-user.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class CreateManageUserComponent implements OnInit {

  public createUserManageForm: FormGroup
  public searchLdapUserForm: FormGroup
  public createLocalUser = true;
  public createLdapUser = true;
  public roles = [];
  public result: any;

  constructor(public formBuilder: FormBuilder, public manageUserService: ManageUserService,
              public simpleModalService: SimpleModalService, public router: Router) { 
    this.createUserManageForm = formBuilder.group({
      userId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])],
      username: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator])],
      password: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator])],
      confirmPassword: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator])],
      userRole: ['', Validators.required ]
    }, {validator: this.checkIfMatchingPasswords('password', 'confirmPassword')})
   
    this.searchLdapUserForm = formBuilder.group({
      userId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])],
      username: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator])],
      userRole: ['', Validators.required ]
    })
    
    this.fetchRoleList();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  checkIfMatchingPasswords(password: string, confirmPassword: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[password],
          passwordConfirmationInput = group.controls[confirmPassword];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notSame: true})
      }
      else {
          return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  ngOnInit() {}

  fetchRoleList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.manageUserService.fetchRoleList().subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          this.result = data["body"];
          this.roles = this.result['roles'];
          console.log("Role List: "+ JSON.stringify(this.roles));
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  createUser() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      const manageUser: ManageUser = {
        userId: this.createUserManageForm.get('userId').value,
        userName: this.createUserManageForm.get('username').value,
        userRole: this.createUserManageForm.get('userRole').value,
        password: this.createUserManageForm.get('password').value,
        confirmPassword: this.createUserManageForm.get('confirmPassword').value,
        resetPassword: null,
        action: null,
        active: null,
        createdBy: null,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        approvedBy: null,
        approvedDate: null
      }

      this.manageUserService.createUser(manageUser).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
            console.log("Create User Response: "+ JSON.stringify(data));
            this.router.navigate(['manage-user/pending-manage-user']) 
        }     
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
      else { control.markAsTouched(); };
    });
  };

  submitForm() {
    this.markFormTouched(this.createUserManageForm);
    if (this.createUserManageForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.createUserManageForm.getRawValue;
      this.createUser();
    } else {
      console.log("Validation Failed")
    }
  };
}
