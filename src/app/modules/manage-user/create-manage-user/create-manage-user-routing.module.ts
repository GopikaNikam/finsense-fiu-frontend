import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateManageUserComponent } from './create-manage-user.component';

const routes: Routes = [
  {
    path: '',
    component: CreateManageUserComponent,
    data: {
      title: 'Create Manage User',
      icon: 'ti-anchor',
      caption: 'Create Manage User',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateManageUserRoutingModule { }
