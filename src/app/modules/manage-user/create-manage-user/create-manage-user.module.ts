import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateManageUserComponent } from './create-manage-user.component';
import { CreateManageUserRoutingModule } from './create-manage-user-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    CreateManageUserRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ CreateManageUserComponent ]
})
export class CreateManageUserModule { }
