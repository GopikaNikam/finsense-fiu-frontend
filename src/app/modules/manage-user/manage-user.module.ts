import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng-select';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ManageUserRoutingModule } from './manage-user-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
    NgxDatatableModule,
    ManageUserRoutingModule
  ],
  declarations: [],
  providers: [SelectOptionService]
})
export class ManageUserModule { }
