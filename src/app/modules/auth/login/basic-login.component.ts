import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserLoginService } from 'src/app/services/user-login/user-login.service';
import { Subscription } from 'rxjs';
import { UserLogin } from 'src/app/services/user-login/user-login';
import { ConfigService } from 'src/app/services/config/config-service';

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit{

  public loginForm: FormGroup;
  public subscription: Subscription;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public result: any;
  public response: any;
  public error = "";
  public permissionsResponse: any;
  public userRole: string;
  public role: string;
  public imagePath: string;
  permissionNames:any = [];

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userLoginService: UserLoginService,
    private configService: ConfigService) {
      this.imagePath = this.configService.logoImagePath;
    }

  ngOnInit() {
      this.loginForm = this.formBuilder.group({
        username: ['', Validators.compose([Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")])],
        password: ['', Validators.compose([Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")])]
    });
    
    sessionStorage.clear();
    this.router.navigate(['/login']);
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  
  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  userLogin() {
    const userLogin: UserLogin = {
      userId: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value
    }

    this.subscription = this.userLoginService.userLogin(userLogin).subscribe(data => {
      this.subscription.unsubscribe();
      this.response = data['body'];
      console.log("User Login Reponse: ", JSON.stringify(data['body']));
      sessionStorage.setItem("authorizationToken", "Bearer: "+ this.response['token']);
      this.userPermissions();
    });
  }

  userPermissions() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.userLoginService.userPermissions().subscribe(data => {
        this.permissionsResponse = data['body'];
        this.userRole = this.permissionsResponse['role'];
        sessionStorage.setItem("userRole", this.userRole);
        sessionStorage.setItem("userPermissions",JSON.stringify(this.permissionsResponse['permissions']));
        console.log("Users Permissions Response:", this.permissionsResponse);

        this.permissionsResponse['permissions'].map(item => {
          return {
              permissionName: item.permissionName
          }
        }).forEach(item => this.permissionNames.push(item));
        sessionStorage.setItem("userPermissionNames",JSON.stringify(this.permissionNames));
        this.router.navigate([this.returnUrl]);
      });
    }else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
      else { control.markAsTouched(); };
    });
  };

  submitForm() {
    this.markFormTouched(this.loginForm);
    if (this.loginForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.loginForm.getRawValue;
      sessionStorage.setItem('currentUser', this.f.username.value);
      this.userLogin();
    } else {
    }
  };
}
