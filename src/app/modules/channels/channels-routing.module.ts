import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Channels',
      status: false
    },
    children: [
      {
        path: 'create-channel',
        loadChildren: './create-channel/create-channel.module#CreateChannelModule'
      },
      {
        path: 'approve-channel',
        loadChildren: './approve-channel/approve-channel.module#ApproveChannelModule'
      },
      {
        path: 'pending-channel',
        loadChildren: './pending-channel/pending-channel.module#PendingChannelModule'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChannelsRoutingModule { }
