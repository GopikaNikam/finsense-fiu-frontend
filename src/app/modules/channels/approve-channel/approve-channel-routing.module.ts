import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveChannelComponent } from './approve-channel.component'

const routes: Routes = [
  {
    path: '',
    component: ApproveChannelComponent,
    data: {
      title: 'Approve Channel',
      icon: 'ti-anchor',
      caption: 'Approve Channel.',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveChannelRoutingModule { }
