import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveChannelComponent } from './approve-channel.component';

describe('ApproveChannelComponent', () => {
  let component: ApproveChannelComponent;
  let fixture: ComponentFixture<ApproveChannelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveChannelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveChannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
