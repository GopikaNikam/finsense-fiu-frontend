import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateChannelComponent } from './create-channel.component'

const routes: Routes = [
  {
    path: '',
    component: CreateChannelComponent,
    data: {
      title: 'Create Channel',
      icon: 'ti-anchor',
      caption: 'Create Channel.',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateChannelRoutingModule { }
