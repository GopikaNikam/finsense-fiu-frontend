import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateChannelRoutingModule } from './create-channel-routing.module';
import { SelectModule } from 'ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CreateChannelComponent, NgbdModalChannel, NgbdModalCreateChannel } from './create-channel.component';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    CreateChannelRoutingModule,
    SelectModule,
    NgxDatatableModule
  ],
  declarations: [ CreateChannelComponent, NgbdModalCreateChannel, NgbdModalChannel ],
  providers: [ SelectOptionService ],
  bootstrap: [ CreateChannelComponent ],
  entryComponents: [ NgbdModalCreateChannel, NgbdModalChannel ]
})
export class CreateChannelModule { }
