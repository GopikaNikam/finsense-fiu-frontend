import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { Channel } from 'src/app/services/admin-function-services/channel/channel';
import { ChannelService } from 'src/app/services/admin-function-services/channel/channel.service';
import * as uuid from 'uuid';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-channel',
  styleUrls: ['./create-channel.component.scss',
              '../../../../assets/icon/icofont/css/icofont.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">{{channelId}} is already exists.</p>
      <button type="button" 
              class="btn btn-outline-primary mb-2 mr-2" 
              (click)="modal.dismiss('cancel click')"
              style="margin-top:18px">
            Ok
      </button>
  </div>`
})

export class NgbdModalChannel {
  @Input() channelId: string;
  constructor(public modal: NgbActiveModal) {}
}

@Component({
  selector: 'ngbd-modal-create-channel',
  styleUrls: ['./create-channel.component.scss',
              '../../../../assets/icon/icofont/css/icofont.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Your channel has been created.</p>
      <button type="button" 
              class="btn btn-outline-primary mb-2 mr-2" 
              [routerLink]="['/channels/pending-channel/']"
              (click)="modal.dismiss('cancel click')"
              style="margin-top:18px">
            Ok
      </button>
  </div>`
})

export class NgbdModalCreateChannel {
  constructor(public modal: NgbActiveModal) {}
}

@Component({
  selector: 'app-create-channel',
  templateUrl: './create-channel.component.html',
  styleUrls: ['./create-channel.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class CreateChannelComponent implements OnInit {

  public channelForm;
  public profileNamesList: any; channelList: any;
  public selectedChannelEnabled: string;
  public isChannelExist: boolean = false;

  constructor(private channelService: ChannelService,
              private formBuilder: FormBuilder, 
              private router: Router, 
              private modalService: NgbModal, 
              private simpleModalService: SimpleModalService) {

    this.channelForm = this.formBuilder.group({
      channelId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.channelNameIdLength])],
      channelName: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator, this.channelNameIdLength])],
      profileName: ['', Validators.required],
      channelEnabled: ['', Validators.required]
    })
    this.fetchChannelList();
  }
  
  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  public channelNameIdLength(control: FormControl) {
    const islength = (control.value || '').trim().length > 32;
    const isValid = !islength;
    return isValid ? null : { 'channelNameIdLength': true };
  }
    
  ngOnInit() {}

  fetchChannelList() {
    this.channelService.fetchChannelList().subscribe(data => {
      console.log(" Channel List Response: ", JSON.stringify(data));
      var response = data['body'];
      this.channelList = response['channel'];
      this.profileNamesList = response['profileNameList'];
    })
  }

  createChannelRequest() {
    if(sessionStorage.getItem("authorizationToken") != null) { 
      this.isChannelExist = this.checkChannelExistence(this.channelForm.get('channelId').value);
      console.log(this.isChannelExist);

      const channel: Channel = {
        channelId: this.channelForm.get('channelId').value,
        channelName: this.channelForm.get('channelName').value,
        profileId: this.channelForm.get('profileName').value,
        enabled: this.selectedChannelEnabled,
        keepAliveTimeout: 0,
        keepAliveUrl: null,	
        profileName: this.channelForm.get('profileName').value,
        action: null,
        active: null,
        createdBy: null,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        approvedBy: null,
        approvedDate: null
      }
      console.log("Channel Request: "+ JSON.stringify(channel));
      this.createChannelResponse(channel);
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  createChannelResponse(channel) {
    this.channelService.createChannel(channel).subscribe(data => {
      console.log("Channel Response: "+ JSON.stringify(data));
      if(data['errors']) {
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
          this.simpleModalService.addModal(AlertComponent, {
            message: result["errorMsg"]
          })
        }
      } else if(data['body']) {
        this.channelForm.reset();
        this.openCreateChannelModal();
        this.fetchChannelList();
      } else {
        if (typeof data["body"] == 'undefined') {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Data not found'
          })
        }
      }     
    })
  }
  
  onChannelChange(event){
    if (event.target.checked) {
      this.selectedChannelEnabled = "Y";
    } else {
        this.selectedChannelEnabled = "N";
    }
  }

  openCreateChannelModal() {
    this.modalService.open(NgbdModalCreateChannel,{ centered: true });
  }

  checkChannelExistence(channelId: string):boolean {
    return this.channelList.some(r => r.channelId === channelId);
  }

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
      else { control.markAsTouched(); };
    });
  };

  submitForm() {
    this.markFormTouched(this.channelForm);
    if (this.channelForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.channelForm.getRawValue;
      this.createChannelRequest();
    } else {
      console.log("Validation Failed")
    }
  };
}