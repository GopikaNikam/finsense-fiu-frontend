import { CreateChannelModule } from './create-channel.module';

describe('CreateChannelModule', () => {
  let createChannelModule: CreateChannelModule;

  beforeEach(() => {
    createChannelModule = new CreateChannelModule();
  });

  it('should create an instance', () => {
    expect(createChannelModule).toBeTruthy();
  });
});
