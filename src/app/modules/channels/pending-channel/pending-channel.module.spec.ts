import { PendingChannelModule } from './pending-channel.module';

describe('PendingChannelModule', () => {
  let pendingChannelModule: PendingChannelModule;

  beforeEach(() => {
    pendingChannelModule = new PendingChannelModule();
  });

  it('should create an instance', () => {
    expect(pendingChannelModule).toBeTruthy();
  });
});
