import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingChannelComponent } from './pending-channel.component'

const routes: Routes = [
  {
    path: '',
    component: PendingChannelComponent,
    data: {
      title: 'Pending Channel',
      icon: 'ti-anchor',
      caption: 'Pending Channel.',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PendingChannelRoutingModule { }
