import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbdModalApprove, NgbdModalReject, PendingChannelComponent } from './pending-channel.component';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
import { PendingChannelRoutingModule } from './pending-channel-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    PendingChannelRoutingModule,
    SelectModule,
    NgxDatatableModule  
  ],
  declarations: [ PendingChannelComponent, NgbdModalApprove, NgbdModalReject ],
  providers: [ SelectOptionService ], 
  bootstrap: [ PendingChannelComponent ],
  entryComponents: [ NgbdModalApprove, NgbdModalReject ]
})
export class PendingChannelModule { }
