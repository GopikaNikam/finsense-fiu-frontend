import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingChannelComponent } from './pending-channel.component';

describe('PendingChannelComponent', () => {
  let component: PendingChannelComponent;
  let fixture: ComponentFixture<PendingChannelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingChannelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingChannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
