import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Profiles',
      status: false
    },
    children: [
      {
        path: 'create-profile',
        loadChildren: './create-profile/create-profile.module#CreateProfileModule'
      },
      {
        path: 'pending-profile',
        loadChildren: './pending-profile/pending-profile.module#PendingProfileModule'
      },
      {
        path: 'approve-profile',
        loadChildren: './approve-profile/approve-profile.module#ApproveProfileModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesRoutingModule { }
