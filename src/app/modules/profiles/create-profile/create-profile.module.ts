import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '/home/gopikan/mpm/finvu/finsense-web/finsense-fiu-frontend/src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CreateProfileComponent, NgbdModalCreateProfile, NgbdModalProfile } from './create-profile.component';
import { CreateProfileRoutingModule } from './create-profile-routing.module';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    CreateProfileRoutingModule,
    SelectModule,
    NgxDatatableModule
  ],
  declarations: [ CreateProfileComponent, NgbdModalCreateProfile, NgbdModalProfile ],
  providers: [ SelectOptionService ],
  bootstrap: [ CreateProfileComponent ],
  entryComponents: [ NgbdModalCreateProfile, NgbdModalProfile ]
})
export class CreateProfileModule { }
