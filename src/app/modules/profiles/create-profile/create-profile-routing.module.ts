import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateProfileComponent } from './create-profile.component';

const routes: Routes = [
  {
    path: '',
    component: CreateProfileComponent,
    data: {
      title: 'Create Profile',
      icon: 'ti-anchor',
      caption: 'Create Profile.',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateProfileRoutingModule { }
