import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { Profile, IDataOption } from 'src/app/services/admin-function-services/profile/profile';
import { ProfileService } from 'src/app/services/admin-function-services/profile/profile.service';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-profile',
  styleUrls: ['./create-profile.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
    `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">{{profileId}} is already exists.</p>
      <button type="button" 
              class="btn btn-outline-primary mb-2 mr-2" 
              (click)="modal.dismiss('cancel click')"
              style="margin-top:18px">
            Ok
      </button>
  </div>`
})

export class NgbdModalProfile {
  @Input() profileId: string;
  constructor(public modal: NgbActiveModal) { }
}

@Component({
  selector: 'ngbd-modal-create-profile',
  styleUrls: ['./create-profile.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
    `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Your profile has been created.</p>
      <button type="button" 
              class="btn btn-outline-primary mb-2 mr-2" 
              [routerLink]="['/profiles/pending-profile/']"
              (click)="modal.dismiss('cancel click')"
              style="margin-top:18px">
            Ok
      </button>
  </div>`
})

export class NgbdModalCreateProfile {
  constructor(public modal: NgbActiveModal) { }
}

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class CreateProfileComponent implements OnInit {

  public profileForm: FormGroup;
  public profileList: any; profileApiUrlList:any = []; checkBoxList = [];

  constructor(private profileService: ProfileService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal,
    private simpleModalService: SimpleModalService) {

    this.profileForm = this.formBuilder.group({
      profileId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.profileNameIdLength])],
      profileName: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.profileNameIdLength])],
      profileDescription: ['', Validators.required],
      profileAPIs: ['', Validators.required]
    })
    this.fetchProfileList();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  public profileNameIdLength(control: FormControl) {
    const islength = (control.value || '').trim().length > 32;
    const isValid = !islength;
    return isValid ? null : { 'profileNameIdLength': true };
  }

  ngOnInit() { }

  fetchProfileList() {
    this.profileService.fetchProfileList().subscribe(data => {
      console.log(" Profile List Response: ", JSON.stringify(data));
      var response = data['body'];
      this.profileList = response['profiles'];
      this.profileApiUrlList = response['profileApiUrls'];
    })
  }

  createProfileRequest() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      const profile: Profile = {
        profileId: this.profileForm.get('profileId').value,
        profileName: this.profileForm.get('profileName').value,
        profileDescription: this.profileForm.get('profileDescription').value,
        profileApis: this.checkBoxList,
        action: null,
        active: null,
        createdBy: null,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        approvedBy: null,
        approvedDate: null,
      }
      console.log("Profile Request: " + JSON.stringify(profile));
      this.createProfileResponse(profile);
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  createProfileResponse(profile) {
    this.profileService.createProfile(profile).subscribe(data => {
      console.log("Profile Response: " + JSON.stringify(data));
      if (data['errors']) {
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
          this.simpleModalService.addModal(AlertComponent, {
            message: result["errorMsg"]
          })
        }
      } else if (data['body']) {
        this.profileForm.reset();
        this.openCreateProfileModal();
        this.fetchProfileList();
      } else {
        if (typeof data["body"] == 'undefined') {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Data not found'
          })
        }
      }
    })
  }

  onCheckboxChange(option: IDataOption, event: any) {
    if (event.target.checked) {
      this.checkBoxList.push(option);
    }
    else {
      for (var i = 0; i < this.checkBoxList.length; i++) {
        if (this.checkBoxList[i] == option) {
          this.checkBoxList.splice(i, 1);
        }
      }
    }
  }

  isSelected(option) {
    return this.checkBoxList.indexOf(option) >= 0;
  }

  openCreateProfileModal() {
    this.modalService.open(NgbdModalCreateProfile, { centered: true });
  }

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control); }
      else { control.markAsTouched(); };
    });
  };

  submitForm() {
    this.markFormTouched(this.profileForm);
    if (this.profileForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.profileForm.getRawValue;
      this.createProfileRequest();
    } else {
      console.log("Validation Failed")
    }
  };
}