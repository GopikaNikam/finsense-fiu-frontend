import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PendingProfileRoutingModule } from './pending-profile-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbdModalApprove, NgbdModalReject, PendingProfileComponent } from './pending-profile.component';
import { SelectModule } from 'ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        PendingProfileRoutingModule,
        SelectModule,
        NgxDatatableModule
    ],
    declarations: [PendingProfileComponent, NgbdModalApprove, NgbdModalReject],
    providers: [SelectOptionService],
    bootstrap: [PendingProfileComponent],
    entryComponents: [NgbdModalApprove, NgbdModalReject]
})
export class PendingProfileModule { }
