import { PendingProfileModule } from './pending-profile.module';

describe('PendingProfileModule', () => {
  let pendingProfileModule: PendingProfileModule;

  beforeEach(() => {
    pendingProfileModule = new PendingProfileModule();
  });

  it('should create an instance', () => {
    expect(pendingProfileModule).toBeTruthy();
  });
});
