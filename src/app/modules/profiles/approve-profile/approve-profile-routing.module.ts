import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveProfileComponent } from './approve-profile.component';

const routes: Routes = [
  {
    path: '',
    component: ApproveProfileComponent,
    data: {
      title: 'Approve Profile',
      icon: 'ti-anchor',
      caption: 'Approve Profile.',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveProfileRoutingModule { }
