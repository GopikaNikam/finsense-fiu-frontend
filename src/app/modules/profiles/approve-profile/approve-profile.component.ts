import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { IDataOption, Profile } from 'src/app/services/admin-function-services/profile/profile';
import { ProfileService } from 'src/app/services/admin-function-services/profile/profile.service';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-delete',
  styleUrls: ['./approve-profile.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template: 
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to delete profile</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-cancel mb-2 mr-2" 
                    (click)="modal.dismiss('cancel click')">
                    Cancel
            </button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="deleteProfile(profileId)">
                    Delete
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalDelete {
 @Input() profileId;

  constructor(public modal: NgbActiveModal, 
              private router: Router, 
              private simpleModalService: SimpleModalService, 
              private profileService: ProfileService) {}
            
    deleteProfile(profileId) {
      if(sessionStorage.getItem("authorizationToken") != null) {
        this.profileService.deleteApprovedProfile(profileId).subscribe(data => { 
          console.log("Deleted approved profile response: "+ data['body']);
          if(data['errors']) {
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else {
            this.simpleModalService.addModal(AlertComponent, {
              message: "Profile has been deleted"
            })
          }      
          this.modal.close();
          this.router.navigate(['/profiles/pending-profile/']);
        });
      } else {
          this.router.navigate(['/login']);
          console.log("Authorization token is null");
      }
    }
}

@Component({
  selector: 'app-approve-profile',
  templateUrl: './approve-profile.component.html',
  styleUrls: ['./approve-profile.component.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class ApproveProfileComponent implements OnInit {

  public updateProfileForm: FormGroup;
  public profileList: any = []; checkBoxList = [];  profileApiUrls = []; profileRowData: any; creationDate: any; CheckBoxesList: any;
  public profilesList = true; viewProfile = true; editProfile = true;
  public profileId: string; createdBy: string;    

  constructor(private profileService: ProfileService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modalService: NgbModal,
    private simpleModalService: SimpleModalService) {
    this.updateProfileForm = this.formBuilder.group({
      updateProfileId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.profileNameIdLength])],
      updateProfileName: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.profileNameIdLength])],
      updateProfileDescription: ['', Validators.required],
      updateProfileApis: ['', Validators.required]
    })
    this.fetchApprovedProfileList();
  }

  ngOnInit() {
  }

  openDeleteModal(profileId: string) {
    const deleteModalRef =  this.modalService.open(NgbdModalDelete,{ centered: true });
    deleteModalRef.componentInstance.profileId = profileId;
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  public profileNameIdLength(control: FormControl) {
    const islength = (control.value || '').trim().length > 32;
    const isValid = !islength;
    return isValid ? null : { 'profileNameIdLength': true };
  }

  fetchApprovedProfileList() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.profileService.fetchApprovedProfileList().subscribe(data => {
        var response = data['body']
        console.log("Approved Profile List Response: " + JSON.stringify(response));
        this.profileApiUrls = response['profileApiUrls'];
        this.profileList = response['profiles'];
        this.profileList = this.profileList.filter(a => a.active == 'Y');
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  viewProfileRecord(profileId) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.profileService.fetchApprovedProfile(profileId).subscribe(data => {
        var response = data['body'];
        console.log("Profile Response: " + response['profile']);
        this.viewProfileDetails(response['profile']);
        this.profilesList = false;
        this.viewProfile = false;
      });
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  editProfileRecord(profileId) {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.profileService.fetchApprovedProfile(profileId).subscribe(data => {
        console.log("Approved Profile Response: " + JSON.stringify(data));
        var response = data['body'];
        this.getUpdateRow(response['profile']);
        this.profilesList = false;
        this.viewProfile = true;
        this.editProfile = false;
      });
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  getUpdateRow(profileData) {
    this.profileId = profileData.profileId;
    this.createdBy = profileData.createdBy,
    this.creationDate = profileData.creationDate   
    this.checkBoxList = profileData.profileApis;

    this.updateProfileForm.setValue({
      updateProfileId: profileData.profileId,
      updateProfileName: profileData.profileName,
      updateProfileDescription: profileData.profileDescription,
      updateProfileApis: this.checkBoxList
    })
  }

  updateProfile() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      const profile: Profile = {
        profileId: this.updateProfileForm.get('updateProfileId').value,
        profileName: this.updateProfileForm.get('updateProfileName').value,
        profileDescription: this.updateProfileForm.get('updateProfileDescription').value,
        profileApis: this.checkBoxList,
        action: null,
        active: null,
        createdBy: this.createdBy,
        creationDate: this.creationDate,
        updatedBy: null,
        updationDate: null,
        approvedBy: null,
        approvedDate: null
      }

      if (sessionStorage.getItem("authorizationToken") != null) {
        this.profileService.updateProfile(profile).subscribe(data => {
          if (data['errors']) {
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else if (data['body']) {
            this.simpleModalService.addModal(AlertComponent, {
              message: "Channel has been updated"
            })
          } else {
            if (typeof data["body"] == 'undefined') {
              this.simpleModalService.addModal(AlertComponent, {
                message: 'Data not found'
              })
            }
          }
          console.log(" Update Profile Response: ", JSON.stringify(data));
          this.fetchApprovedProfileList();
          this.router.navigate(['/profiles/pending-profile'])
        })
      } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
      }
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  viewProfileDetails(profileData) {
    this.profileRowData = profileData;
  }

  toggleCloseProfileRecord() {
    this.profilesList = true;
    this.viewProfile = true;
  }

  toggleEditProfile() {
    this.editProfile = true;
    this.profilesList = true;
    this.viewProfile = true;
  }

  onCheckboxChange(option: IDataOption, event:any) {
    if(event.target.checked) {
      this.checkBoxList.push(option);
    }
    else {
      for(var i=0 ; i < this.checkBoxList.length; i++) {
        if(this.checkBoxList[i] == option){
          this.checkBoxList.splice(i,1);
        }
      }
    }
    this.CheckBoxesList = this.checkBoxList;
  }
  
  isSelected(option){
      return this.checkBoxList.indexOf(option) >= 0;
  }

  submitForm() {
    this.markFormTouched(this.updateProfileForm);
    if (this.updateProfileForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.updateProfileForm.getRawValue;
      this.updateProfile();
    } else {
      alert("Validation Failed");
    }
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) {
        control.markAsTouched();
        this.markFormTouched(control);
      } else {
        control.markAsTouched();
      };
    });
  };
}
