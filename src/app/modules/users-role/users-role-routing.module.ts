import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Users',
      status: false
    },
    children: [
      {
        path: 'approve-users-role',
        loadChildren: './approve-users-role/approve-users-role.module#ApproveUsersRoleModule'
      },
      {
        path: 'create-users-role',
        loadChildren: './create-users-role/create-users-role.module#CreateUsersRoleModule'
      },
      {
        path: 'pending-users-role',
        loadChildren: './pending-users-role/pending-users-role.module#PendingUsersRoleModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoleRoutingModule { }
