import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PendingUsersRoleRoutingModule } from './pending-users-role-routing.module';
import { PendingUsersRoleComponent, NgbdModalApprove, NgbdModalReject } from './pending-users-role.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';

@NgModule({
  imports: [
    CommonModule,
    PendingUsersRoleRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  declarations: [ PendingUsersRoleComponent, NgbdModalApprove, NgbdModalReject ],
  providers: [ SelectOptionService ], 
  bootstrap: [ PendingUsersRoleComponent ],
  entryComponents: [ NgbdModalApprove, NgbdModalReject ]
})
export class PendingUsersRoleModule { }
