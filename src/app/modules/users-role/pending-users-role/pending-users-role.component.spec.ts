import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingUsersRoleComponent } from './pending-users-role.component';

describe('PendingUsersRoleComponent', () => {
  let component: PendingUsersRoleComponent;
  let fixture: ComponentFixture<PendingUsersRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingUsersRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingUsersRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
