import { PendingUsersRoleModule } from './pending-users-role.module';

describe('PendingUsersRoleModule', () => {
  let pendingUsersRoleModule: PendingUsersRoleModule;

  beforeEach(() => {
    pendingUsersRoleModule = new PendingUsersRoleModule();
  });

  it('should create an instance', () => {
    expect(pendingUsersRoleModule).toBeTruthy();
  });
});
