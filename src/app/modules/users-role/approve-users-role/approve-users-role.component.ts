import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { UserRole, IDataOption } from 'src/app/services/user-role/user-role';
import { SimpleModalService } from 'ngx-simple-modal';
import { UserRoleService } from 'src/app/services/user-role/user-role.service';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-delete',
  styleUrls: ['./approve-users-role.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template: 
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to delete consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="deleteApprovedUserRole(roleName)">
                    Delete
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalDelete {
 @Input() roleName;
 
  constructor(public modal: NgbActiveModal, public router: Router, 
              public simpleModalService: SimpleModalService, 
              public userRoleService: UserRoleService) {}

  deleteApprovedUserRole(roleName) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.userRoleService.deleteApprovedUserRole(roleName).subscribe(data => { 
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          this.simpleModalService.addModal(AlertComponent, {
            message: "User has role been deleted"
          })
        }    
        this.modal.close();
        this.router.navigate(['/users-role/pending-users-role/']);
        console.log("Deleted Approved User Role Response: "+ JSON.stringify(data));
      });
    } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
    }
  }
}

@Component({
  selector: 'app-approve-users-role',
  templateUrl: './approve-users-role.component.html',
  styleUrls: ['./approve-users-role.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class ApproveUsersRoleComponent implements OnInit {

  public updateUserRoleForm: FormGroup;
  public userRoleList = true; viewUserRole = true; editUserRole = true;
  public permissionGroups = []; checkBoxList = [];
  public userRole: any; approvedUserRole: any; public approvedUserRoleList: any;  
         approvedUserRoleResult: any

  constructor(public formBuilder: FormBuilder, public userRoleService: UserRoleService,
              public router: Router, public simpleModalService: SimpleModalService,
              public modalService: NgbModal) {
    this.updateUserRoleForm = formBuilder.group({
      updateRoleName: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])],
      updateRoleDescription: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])],
      updateSelectPermission: [Validators.required]
    }) 
    this.fetchApprovedUserRoleList();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  ngOnInit() {}

  openDeleteModal(roleName: string) {
    const deleteModalRef =  this.modalService.open(NgbdModalDelete,{ centered: true });
    deleteModalRef.componentInstance.roleName = roleName;
  }

  fetchApprovedUserRoleList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.userRoleService.fetchApprovedUserRoleList().subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          this.approvedUserRoleResult = data['body']
          this.approvedUserRoleList = this.approvedUserRoleResult.userRoles.filter(a => a.active == 'Y');
          this.permissionGroups = this.approvedUserRoleResult.permissionGroups.sort((a,b) => a.permissionGroup.localeCompare(b.permissionGroup));
          console.log("PermissionGroups: "+ JSON.stringify(this.permissionGroups));
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }
  
  fetchApprovedUserRole(roleName) {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.userRoleService.fetchApprovedUserRole(roleName).subscribe(data => {
        console.log("Approved User Role Response: "+ JSON.stringify(data));
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          this.viewUserRoles(data["body"]);
          this.userRoleList = false;
          this.viewUserRole = false;
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }
  
  updateApprovedUserDetails() {
  
    if(this.checkBoxList == null || this.checkBoxList.length == 0) {
      this.simpleModalService.addModal(AlertComponent, {
        message: 'Please select atleast one role permission' ,
      }) 
    } else {
      if(sessionStorage.getItem("authorizationToken") != null) {
        const userRole: UserRole = {
          roleName: this.updateUserRoleForm.get('updateRoleName').value,
          roleDescription: this.updateUserRoleForm.get('updateRoleDescription').value,
          permissionGroupList: this.checkBoxList,
          permissions: null,
          permissionGroups: null,
          action: null,
          active: null,
          createdBy: null,
          creationDate: null,
          updatedBy: null,
          updationDate: null,
          approvedBy: null,
          approvedDate: null
        }
  
      this.userRoleService.updateApprovedUserRole(userRole).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
            console.log("Update User Role Response: "+ JSON.stringify(data));
            this.router.navigate(['/users-role/pending-users-role'])
          }
        })
      } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
      }
    }
  }
  
  editUserRoleRecord(roleName) {
    if(sessionStorage.getItem("authorizationToken") != null) { 
      this.userRoleService.fetchApprovedUserRole(roleName).subscribe(data => {
        this.getUpdateRow(data["body"]);
        this.userRoleList = false;
        this.viewUserRole = true;
        this.editUserRole = false;
        console.log("Update Approved User Role Response: "+ JSON.stringify(data));
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  viewUserRoles(userRole){
    this.approvedUserRole = userRole.userRole;
  }

  getUpdateRow(data) {
    this.checkBoxList = data.userRole.permissionGroupList;
    this.updateUserRoleForm.setValue({
      updateRoleName: data.userRole.roleName,
      updateRoleDescription: data.userRole.roleDescription,
      updateSelectPermission: this.checkBoxList
    })
  }

  onCheckBoxChange(option: IDataOption, event:any) {
    if(event.target.checked) {
      this.checkBoxList.push(option);
      console.log("Checked PermissionGroup: "+ JSON.stringify(this.checkBoxList))
    }
    else {
      for(var i=0 ; i < this.checkBoxList.length; i++) {
        if(this.checkBoxList[i] == option){
          this.checkBoxList.splice(i,1);
        }
      }
      console.log("Unchecked PermissionGroup: "+ JSON.stringify(this.checkBoxList))
    }
  }
  
  isSelected(option) {
    return this.checkBoxList.indexOf(option) >= 0;
  }

  submitForm() {
    this.markFormTouched(this.updateUserRoleForm);
    if (this.updateUserRoleForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.updateUserRoleForm.getRawValue;
      this.updateApprovedUserDetails();
    } else {
      alert("Validation Failed");
    }
  };
  
  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
        else { control.markAsTouched(); };
    });
  };

  toggleUser() {
    this.editUserRole = true;
    this.userRoleList = true;
    this.viewUserRole = true;
  }

  toggleCloseUserRecord(){
    this.userRoleList = true;
    this.viewUserRole = true;
  }
}
