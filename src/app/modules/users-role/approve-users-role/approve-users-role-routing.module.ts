import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveUsersRoleComponent } from './approve-users-role.component';

const routes: Routes = [
  {
    path: '',
    component: ApproveUsersRoleComponent,
    data: {
      title: 'Approve Users Role',
      icon: 'ti-anchor',
      caption: 'Approve Users Role',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveUsersRoleRoutingModule { }
