import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApproveUsersRoleRoutingModule } from './approve-users-role-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ApproveUsersRoleComponent, NgbdModalDelete } from './approve-users-role.component';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
import { SelectModule } from 'ng-select';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    NgxDatatableModule,
    SelectModule,
    ApproveUsersRoleRoutingModule
  ],
  declarations: [ ApproveUsersRoleComponent, NgbdModalDelete ],
  providers: [ SelectOptionService ], 
  bootstrap: [ ApproveUsersRoleComponent ],
  entryComponents: [ NgbdModalDelete ]
})
export class ApproveUsersRoleModule { }

