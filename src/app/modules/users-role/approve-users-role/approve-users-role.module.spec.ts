import { ApproveUsersRoleModule } from './approve-users-role.module';

describe('ApproveUsersRoleModule', () => {
  let approveUsersRoleModule: ApproveUsersRoleModule;

  beforeEach(() => {
    approveUsersRoleModule = new ApproveUsersRoleModule();
  });

  it('should create an instance', () => {
    expect(approveUsersRoleModule).toBeTruthy();
  });
});
