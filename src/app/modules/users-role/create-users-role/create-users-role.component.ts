import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { AlertComponent } from '../../alert/alert.component';
import { SimpleModalService } from 'ngx-simple-modal';
import { Router } from '@angular/router';
import { IDataOption, UserRole } from 'src/app/services/user-role/user-role';
import { UserRoleService } from 'src/app/services/user-role/user-role.service';

@Component({
  selector: 'app-create-users-role',
  templateUrl: './create-users-role.component.html',
  styleUrls: ['./create-users-role.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class CreateUsersRoleComponent implements OnInit {

  public createUserRoleForm: FormGroup;
  public permissionGroups = []; checkBoxList = [];
  public permissionGroupsResult: any

  constructor(public formBuilder: FormBuilder, public userRoleService: UserRoleService,
              public simpleModalService: SimpleModalService, public router: Router) {

    this.createUserRoleForm = formBuilder.group({
      roleName: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])],
      selectPermission: ['', Validators.required],
      roleDescription: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])]
    })
    this.fetchUserRoleList();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  ngOnInit() {}

  fetchUserRoleList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.userRoleService.fetchUserRoleList().subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
            this.permissionGroupsResult = data['body']
            this.permissionGroups = this.permissionGroupsResult.permissionGroups.sort((a,b) => a.permissionGroup.localeCompare(b.permissionGroup));
            console.log("PermissionGroups: "+ JSON.stringify(this.permissionGroupsResult));
        }
      })
    } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
    }
  }
  
  createUserRole() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      const userRole: UserRole = {
        roleName: this.createUserRoleForm.get('roleName').value,
        roleDescription: this.createUserRoleForm.get('roleDescription').value,
        permissionGroupList: this.checkBoxList,
        permissionGroups: null,
        permissions: null,
        action: null,
        active: null,
        createdBy: null,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        approvedBy: null,
        approvedDate: null
      }

      console.log("Create User Role Request: "+ JSON.stringify(userRole));
      this.userRoleService.createUserRole(userRole).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } else {
          console.log("Create User Role Response: "+ JSON.stringify(data));
          this.router.navigate(['users-role/pending-users-role']) 
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }
  
  onCheckBoxChange(option: IDataOption, event:any) {
    if(event.target.checked) {
      this.checkBoxList.push(option);
    }
    else {
      for(var i=0 ; i < this.checkBoxList.length; i++) {
        if(this.checkBoxList[i] == option){
          this.checkBoxList.splice(i,1);
        }
      }
    }
  }
  
  isSelected(option){
    return this.checkBoxList.indexOf(option) >= 0;
  }

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
      else { control.markAsTouched(); };
    });
  };

  submitForm() {
    this.markFormTouched(this.createUserRoleForm);
    if (this.createUserRoleForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.createUserRoleForm.getRawValue;
      this.createUserRole();
    } else {
      console.log("Validation Failed")
    }
  };
}
