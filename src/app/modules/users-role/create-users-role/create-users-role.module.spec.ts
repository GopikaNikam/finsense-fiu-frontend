import { CreateUsersRoleModule } from './create-users-role.module';

describe('CreateUsersRoleModule', () => {
  let createUsersRoleModule: CreateUsersRoleModule;

  beforeEach(() => {
    createUsersRoleModule = new CreateUsersRoleModule();
  });

  it('should create an instance', () => {
    expect(createUsersRoleModule).toBeTruthy();
  });
});
