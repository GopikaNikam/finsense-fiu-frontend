import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUsersRoleComponent } from './create-users-role.component';

const routes: Routes = [
  {
    path: '',
    component: CreateUsersRoleComponent,
    data: {
      title: 'Create Users Role',
      icon: 'ti-anchor',
      caption: 'Create Users Role',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateUsersRoleRoutingModule { }
