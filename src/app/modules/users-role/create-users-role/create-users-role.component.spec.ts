import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUsersRoleComponent } from './create-users-role.component';

describe('CreateUsersRoleComponent', () => {
  let component: CreateUsersRoleComponent;
  let fixture: ComponentFixture<CreateUsersRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUsersRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUsersRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
