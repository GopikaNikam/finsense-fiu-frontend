import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ConsentTemplateService } from 'src/app/services/admin-function-services/consent-tmplate/consent-template.service';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { ConsentTemplateRequest, Purpose, Category, Frequency, DataLife,
         ConsentExpiry, IDataOption } from 'src/app/services/admin-function-services/consent-tmplate/consent-template';
import { IOption } from 'ng-select';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-delete',
  styleUrls: ['./approve-consent-template.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template: 
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to delete consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-cancel mb-2 mr-2" 
                    (click)="modal.dismiss('cancel click')">
                    Cancel
            </button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="deleteTemplate(templateName)">
                    Delete
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalDelete {
 @Input() templateName;

  constructor(public modal: NgbActiveModal, 
              private router: Router, 
              private simpleModalService: SimpleModalService, 
              private consentTemplateService: ConsentTemplateService ) {}
            
    deleteTemplate(templateName) {
      if(sessionStorage.getItem("authorizationToken") != null) {
        this.consentTemplateService.deleteApprovedConsentTemplate(templateName).subscribe(data => { 
          if(data['errors']) {
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else {
            this.simpleModalService.addModal(AlertComponent, {
              message: "Consent template has been deleted"
            })
          }      
          this.modal.close();
          console.log("Deleted approved consent template response: "+ data['body']);
          this.router.navigate(['/consent-templates/pending-consent-template/']);
        });
      } else {
          this.router.navigate(['/login']);
          console.log("Authorization token is null");
      }
    }
}

@Component({
  selector: 'app-approve-consent-template',
  templateUrl: './approve-consent-template.component.html',
  styleUrls: ['./approve-consent-template.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})

export class ApproveConsentTemplateComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;
 
  public updateConsentTemplateForm: FormGroup;
  public consentTemplatesList = true; viewConsentTemplate = true; viewTemplateButtons = true;
  editConsentTemplate = true; periodic = true;
  public consentTypeList = []; fiTypesList = []; fiTypes = []; consentTypesList = [];
  purposeList = []; checkBoxList = [];
  public CheckBoxesList: any; consentTemplateList: any; consentTemplateRowData; dataRangeStartMonthsUnit: any;
  radioSelValidity:any; purpose: any; selectFiTypes: any;
  public selectedValidity:string; frequencyUnit: String; purposeCode: string; purposeText: string; createdBy: string;
  public frequencyValue: number; selectedDataRangeStartMonths: number;  
  public creationDate: Date;

  fetchTypes = ['ONETIME', 'PERIODIC'];
  frequencies = ['DAY', 'HOUR', 'MONTH', 'YEAR'];
  consentExpiryUnits = [ 'DAY', 'MONTH', 'YEAR' ];
  consentModes = ['QUERY', 'STORE','STREAM','VIEW'];
  dataLifeUnitList = ['DAY', 'INF', 'MONTH', 'YEAR'];
  dateRangeUnits = ['current', 'last 1', 'last 3', 'last 6', 'last 9', 'last 12', 'last 18']
  
  constructor(private selectOptionService: SelectOptionService, 
              private consentTemplateService: ConsentTemplateService,
              private formBuilder: FormBuilder, 
              private router: Router, 
              private modalService: NgbModal,
              private simpleModalService: SimpleModalService) {

    this.updateConsentTemplateForm = this.formBuilder.group({
    updateTemplateName: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.templateNameLength])],
    updateTemplateDescription: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator, this.templateDescriptionLength ])],
    updateConsentMode: ['', Validators.required ],
    updateConsentPurpose: ['', Validators.required],
    updateConsentExpiryValue: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
    updateConsentExpiryUnit: ['', Validators.required],
    updateDataLifeValue: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
    updateDataLifeUnit: ['', Validators.required],
    updateFiTypeList: ['', Validators.required],
    updateCheckBoxConsentType: ['', Validators.required],
    updateFetchType: ['', Validators.required],
    updatePeriodicFrequencyValue:  ['', Validators.compose([Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
    updatePeriodicFrequencyUnit: [''],
    updateConsentStartValue: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
    updateDataRangeStartMonths: ['', Validators.required],
    updateDataRangeEndMonths: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]?([0-9]\d*)?$/)])], 
  }) 
  this.fetchApprovedConsentTemplateList();
}

public noWhitespaceValidator(control: FormControl) {
  const isWhitespace = (control.value || '').trim().length === 0;
  const isValid = !isWhitespace;
  return isValid ? null : { 'whitespace': true };
}

public templateNameLength(control: FormControl) {
  const islength = (control.value || '').trim().length > 32;
  const isValid = !islength;
  return isValid ? null : { 'templateNameLength': true };
}

public templateDescriptionLength(control: FormControl) {
  const islength = (control.value || '').trim().length > 128;
  const isValid = !islength;
  return isValid ? null : { 'templateDescriptionLength': true };
}

ngOnInit() {}

openDeleteModal(templateName: string) {
  const deleteModalRef =  this.modalService.open(NgbdModalDelete,{ centered: true });
  deleteModalRef.componentInstance.templateName = templateName;
}

fetchApprovedConsentTemplateList() {
  if(sessionStorage.getItem("authorizationToken") != null) {
    this.consentTemplateService.fetchApprovedConsentTemplateList().subscribe(data => {
      var response = data['body']
      console.log("Approved ConsentTemplate List Response: "+ response);
      this.fiTypes = response['fiTypes'];
      this.consentTypesList = response['consentTypes'];
      this.fiTypeDropDownList(this.fiTypes);
      var purposeList = response['purpose'];
      purposeList.map(item => {
        return {
          text: item.text,
        }
      }).forEach(item => this.purposeList.push(item.text));
      this.purposeList.sort((a,b) => a.localeCompare(b));
      this.consentTemplateList = response['consentTemplateRequest'];
      this.consentTemplateList = this.consentTemplateList.filter(a => a.active == 'Y');
    })
  } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
}

viewConsentTemplateRecord(templateName) {
  if(sessionStorage.getItem("authorizationToken") != null) {
    this.consentTemplateService.fetchApprovedConsentTemplate(templateName).subscribe(data => {
      var response = data['body']
      console.log("Consent Template Response: "+ response['consentTemplateRequest']);
      this.viewConsentTemplateDetails(response['consentTemplateRequest']);
      this.consentTemplatesList = false;
      this.viewConsentTemplate = false;
    });
  } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
  }
}

editConsentTemplateRecord(templateName) {
  if(sessionStorage.getItem("authorizationToken") != null) {
    this.consentTemplateService.fetchApprovedConsentTemplate(templateName).subscribe(data => {
      var response = data['body'];
      console.log("Approved Consent Template  Response: "+ response['consentTemplateRequest']);
      var result = response['consentTemplateRequest'];
      this.createdBy = result.createdBy;
      this.creationDate = result.creationDate;
      this.getSelectedDataRangeStartMonthLabel(result.dataRangeStartMonths);
      this.getUpdateRow(response['consentTemplateRequest']);
      this.consentTemplatesList = false;
      this.viewConsentTemplate = true;
      this.editConsentTemplate = false;
    });
  } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
  }
}

updateConsentTemplate() {

  if(this.checkBoxList == null || this.checkBoxList.length == 0) {
    this.simpleModalService.addModal(AlertComponent, {
      message: 'Please select atleast one consent type' ,
    })
  } else {
      if(sessionStorage.getItem("authorizationToken") != null) {  

        this.getUpdatedFrequency(this.selectedValidity);
        this.getSelectedDataRangeStartMonths(this.updateConsentTemplateForm.get('updateDataRangeStartMonths').value);

        const frequency: Frequency = {
          unit:  this.frequencyUnit,
          value: this.frequencyValue
        }

        const dataLife: DataLife = {
          unit: this.updateConsentTemplateForm.get('updateDataLifeUnit').value,
          value: Number(this.updateConsentTemplateForm.get('updateDataLifeValue').value),
        }

        const consentExpiry: ConsentExpiry = {
          unit: this.updateConsentTemplateForm.get('updateConsentExpiryUnit').value,
          value: Number(this.updateConsentTemplateForm.get('updateConsentExpiryValue').value),
        }

        const category: Category = {
          type: this.purpose.Category.type
        }
        const purpose: Purpose = {
          code: this.purpose.code,
          refUri: this.purpose.refUri,
          text: this.purpose.text, 
          Category: category
        }

      const consentTemplateRequest: ConsentTemplateRequest = {
        templateName: this.updateConsentTemplateForm.get('updateTemplateName').value,
        templateDescription: this.updateConsentTemplateForm.get('updateTemplateDescription').value,
        consentMode: this.updateConsentTemplateForm.get('updateConsentMode').value,
        consentTypes: this.checkBoxList, 
        fiTypes: this.fiTypesList,
        Purpose: purpose,
        fetchType: this.updateConsentTemplateForm.get('updateFetchType').value,
        Frequency: frequency,
        DataLife: dataLife,
        ConsentExpiry: consentExpiry,
        consentStartDays:  Number(this.updateConsentTemplateForm.get('updateConsentStartValue').value),   
        dataRangeStartMonths: this.selectedDataRangeStartMonths, 
        dataRangeEndMonths: Number(this.updateConsentTemplateForm.get('updateDataRangeEndMonths').value),
        createdBy: null,
        creationDate: null,
        updatedBy: null,
        updationDate: null,
        approvedBy: null,
        approvedDate: null
      }

      if(sessionStorage.getItem("authorizationToken") != null) {
        this.consentTemplateService.updateApprovedConsentTemplate(consentTemplateRequest).subscribe(data => {
          if(data['errors']) {
            var data1 = data['errors'];
            for (let key in data1) {
              var result = data1[key];
              console.log("data error : " + JSON.stringify(result["errorMsg"]));
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          } else if(data['body']) {
            this.simpleModalService.addModal(AlertComponent, {
              message: "Consent template has been updated"
            })
          } else {
            if (typeof data["body"] == 'undefined') {
              this.simpleModalService.addModal(AlertComponent, {
                message: 'Data not found'
              })
            }
          }     
          console.log(" Update Consent Template Response: ", JSON.stringify(data));
          this.fetchApprovedConsentTemplateList();
          this.router.navigate(['/consent-templates/pending-consent-template'])
        })
      } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
      }
      } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
      }
    }
}

getUpdatedConsentTemplatePurpose() {
  if(sessionStorage.getItem("authorizationToken") != null) {
    if(this.updateConsentTemplateForm.get('updateConsentPurpose').value == 'Wealth management service') {
      this.purposeCode = "101"
    } else if(this.updateConsentTemplateForm.get('updateConsentPurpose').value == 'Customer spending patterns, budget or other reportings') {
      this.purposeCode = "102"
    } else if(this.updateConsentTemplateForm.get('updateConsentPurpose').value == 'Aggregated statement') {
      this.purposeCode = "103"
    } else if(this.updateConsentTemplateForm.get('updateConsentPurpose').value == 'Explicit consent for monitoring of the accounts') {
      this.purposeCode = "104"
    } else if(this.updateConsentTemplateForm.get('updateConsentPurpose').value == 'Explicit one-time consent for the accounts') {
      this.purposeCode = "105"
    } else {
      console.log("Invalid purpose");
    }

    this.consentTemplateService.fetchPurpose(this.purposeCode).subscribe(data => {
      console.log("Purpose Response: "+ JSON.stringify(data));
      this.purpose = data['body'];
      if(this.purpose != null) {
          this.updateConsentTemplate();
      } else {
        console.log("Purpose is not found");
      }
    })
  } else {
    this.router.navigate(['/login']);
    console.log("Authorization token is null");
  }
}

getUpdateRow(consentTemplateData) {

  this.selectedValidity = consentTemplateData.fetchType;
  this.purposeText =  consentTemplateData.Purpose.text;
  this.checkBoxList = consentTemplateData.consentTypes;
  this.fiTypesList = consentTemplateData.fiTypes;

  if(this.selectedValidity == 'PERIODIC') {
    this.periodic = false
  } else {
    this.periodic = true;
  }

  this.updateConsentTemplateForm.setValue({
    updateTemplateName: consentTemplateData.templateName,
    updateTemplateDescription: consentTemplateData.templateDescription,
    updateConsentMode: consentTemplateData.consentMode,
    updateConsentPurpose: this.purposeText,
    updateConsentExpiryValue: consentTemplateData.ConsentExpiry.value,
    updateConsentExpiryUnit: consentTemplateData.ConsentExpiry.unit,
    updateDataLifeValue: consentTemplateData.DataLife.value,
    updateDataLifeUnit: consentTemplateData.DataLife.unit,
    updateFiTypeList: consentTemplateData.fiTypes,
    updateCheckBoxConsentType: this.checkBoxList,
    updateFetchType: this.selectedValidity, 
    updatePeriodicFrequencyValue: consentTemplateData.Frequency.value,
    updatePeriodicFrequencyUnit: consentTemplateData.Frequency.unit,
    updateConsentStartValue: consentTemplateData.consentStartDays,
    updateDataRangeStartMonths: this.dataRangeStartMonthsUnit,
    updateDataRangeEndMonths: consentTemplateData.dataRangeEndMonths
  })
}

getSelectedDataRangeStartMonthLabel(dataRangeStartMonths) {
  if(dataRangeStartMonths == 0) {
    this.dataRangeStartMonthsUnit = "current"
  } else if(dataRangeStartMonths == 1) {
    this.dataRangeStartMonthsUnit = "last 1"
  } else if(dataRangeStartMonths == 3) {
    this.dataRangeStartMonthsUnit = "last 3"
  } else if(dataRangeStartMonths == 6) {
    this.dataRangeStartMonthsUnit = "last 6"
  } else if(dataRangeStartMonths == 9) {
    this.dataRangeStartMonthsUnit = "last 9"
  } else if(dataRangeStartMonths == 12) {
    this.dataRangeStartMonthsUnit = "last 12"
  } else if(dataRangeStartMonths == 18) {
    this.dataRangeStartMonthsUnit = "last 18"
  }
  return this.dataRangeStartMonthsUnit;
}

getSelectedDataRangeStartMonths(dataRangeStartMonths) {
  if(dataRangeStartMonths === 'current') {
    this.selectedDataRangeStartMonths = 0
  } else if(dataRangeStartMonths === 'last 1') {
    this.selectedDataRangeStartMonths = 1
  } else if(dataRangeStartMonths === 'last 3') {
    this.selectedDataRangeStartMonths = 3
  } else if(dataRangeStartMonths === 'last 6') {
    this.selectedDataRangeStartMonths = 6
  } else if(dataRangeStartMonths === 'last 9') {
    this.selectedDataRangeStartMonths = 9
  } else if(dataRangeStartMonths === 'last 12') {
    this.selectedDataRangeStartMonths = 12
  } else if(dataRangeStartMonths === 'last 18') {
    this.selectedDataRangeStartMonths = 18
  }
  return this.selectedDataRangeStartMonths;
}

fiTypeDropDownList(fiTypes) {
  this.selectFiTypes = fiTypes.map(data => {
    return {
        label:data.fiType,
        value:data.fiTypeDescription
    };
  }).sort((a,b) => a.label.localeCompare(b.label));
}

onSelected(option: IOption) {
  this.fiTypesList.push(option.label);
  console.log("Selected fitypes:"+ this.fiTypesList);
}

onDeselected(option: IOption) {
  const index: number = this.fiTypesList.indexOf(option.label);
    if (index !== -1) {
      this.fiTypesList.splice(index, 1);
    }
    console.log("Deselected fitypes:"+ this.fiTypesList);
}

getConsentValidity(){
  this.radioSelValidity = this.fetchTypes.find(Item => Item === this.selectedValidity);
    if(this.radioSelValidity === "ONETIME"){
      this.periodic = true;
    } else {
      this.periodic = false;
    }
}

onvalidityChange(item){
  this.getConsentValidity();
}

getUpdatedFrequency(selectedValidity) {
  if(selectedValidity === 'ONETIME') {
    this.frequencyUnit = 'YEAR'
    this.frequencyValue = 0
    this.updateConsentTemplateForm.get('updatePeriodicFrequencyUnit').reset();
    this.updateConsentTemplateForm.get('updatePeriodicFrequencyValue').reset();
  } else {
    this.frequencyUnit = this.updateConsentTemplateForm.get('updatePeriodicFrequencyUnit').value
    this.frequencyValue = Number(this.updateConsentTemplateForm.get('updatePeriodicFrequencyValue').value)
  }
}

viewConsentTemplateDetails(consentTemplateData){
  this.consentTemplateRowData = consentTemplateData;
}

toggleCloseTemplateRecord(){
  this.consentTemplatesList = true;
  this.viewConsentTemplate = true;
}

toggleEditConsentTemplate() {
  this.editConsentTemplate = true;
  this.consentTemplatesList = true;
  this.viewConsentTemplate = true;
}

onCheckboxChange(option: IDataOption, event:any) {
  if(event.target.checked) {
    this.checkBoxList.push(option);
    console.log("Checked Consent Type: "+ JSON.stringify(this.checkBoxList))
  }
  else {
    for(var i=0 ; i < this.checkBoxList.length; i++) {
      if(this.checkBoxList[i] == option){
        this.checkBoxList.splice(i,1);
      }
    }
    console.log("Unchecked Consent Type: "+ JSON.stringify(this.checkBoxList))
  }
  this.CheckBoxesList = this.checkBoxList;
}

isSelected(option){
    return this.checkBoxList.indexOf(option) >= 0;
}

formValidation() {
  var consentExpiry =  Number(this.updateConsentTemplateForm.get('updateConsentExpiryValue').value)
  var dateRangeEndMonths =  Number(this.updateConsentTemplateForm.get('updateDataRangeEndMonths').value)
  var periodicFrequencyValue = Number(this.updateConsentTemplateForm.get('updatePeriodicFrequencyValue').value)

  if(this.frequencyValueValidation(periodicFrequencyValue)) {
    if(this.frequencyValidation()) {
      if(this.consentExpiryValidation(consentExpiry)){
        if(this.dateRangeEndMonthsValidation(dateRangeEndMonths)) {
          this.getUpdatedConsentTemplatePurpose();
        }
      }
    }
  }
  return false;
}

consentExpiryValidation(consentExpiry) {
  if (consentExpiry == 0 ) {
    this.simpleModalService.addModal(AlertComponent, {
      message: 'Consent expiry value can not be negative or 0',
    })
    return false;
  } 
  return true;
}

dateRangeEndMonthsValidation(dateRangeEndMonths) {
  if (dateRangeEndMonths < 0) {
    this.simpleModalService.addModal(AlertComponent, {
      message: 'DateRangeEndMonths can not be negative' ,
    })
    return false;
  } 
  return true;
}

frequencyValidation() {
  if(this.selectedValidity == 'PERIODIC') {
    var periodicFrequencyValue = this.updateConsentTemplateForm.get('updatePeriodicFrequencyValue').value
    var periodicFrequencyUnit = this.updateConsentTemplateForm.get('updatePeriodicFrequencyUnit').value
    if( periodicFrequencyValue == '' || periodicFrequencyUnit  == '') {
      this.simpleModalService.addModal(AlertComponent, {
          message: 'Please enter unit and value frequency use' ,
      })
      return false;
    } else{
      return true;
    }
  } 
  return true;
}

frequencyValueValidation(frequencyValue) {
  if(this.selectedValidity == 'PERIODIC') {
    if (frequencyValue == 0 ) {
      this.simpleModalService.addModal(AlertComponent, {
        message: 'Frequency value can not be negative or 0',
      })
      return false;
    } else{
      return true;
    }
  } 
  return true;
}

submitForm() {
  this.markFormTouched(this.updateConsentTemplateForm);
  if (this.updateConsentTemplateForm.valid) {
    // You will get form value if your form is valid
    var formValues = this.updateConsentTemplateForm.getRawValue;
    this.formValidation();
  } else {
    alert("Validation Failed");
  }
};

markFormTouched(group: FormGroup | FormArray) {
  Object.keys(group.controls).forEach((key: string) => {
    const control = group.controls[key];
    if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
    else { control.markAsTouched(); };
  });
};

}