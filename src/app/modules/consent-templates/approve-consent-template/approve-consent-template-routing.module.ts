import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApproveConsentTemplateComponent } from './approve-consent-template.component'

const routes: Routes = [
  {
    path: '',
    component: ApproveConsentTemplateComponent,
    data: {
      title: 'Approve Consent Template',
      icon: 'ti-anchor',
      caption: 'Approve Consent Templates.',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApproveConsentTemplateRoutingModule { }
