import { ApproveConsentTemplateModule } from './approve-consent-template.module';

describe('ApproveConsentTemplateModule', () => {
  let approveConsentTemplateModule: ApproveConsentTemplateModule;

  beforeEach(() => {
    approveConsentTemplateModule = new ApproveConsentTemplateModule();
  });

  it('should create an instance', () => {
    expect(approveConsentTemplateModule).toBeTruthy();
  });
});
