import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateConsentTemplateComponent, NgbdModalCreateTemplate, NgbdModalTemplate } from './create-consent-template.component';
import { CreateConsentTemplateRoutingModule } from './create-consent-template-routing.module';
import { SelectModule } from 'ng-select';
import { SelectOptionService } from 'src/app/shared/elements/select-option.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    CreateConsentTemplateRoutingModule,
    SelectModule,
    NgxDatatableModule
  ],
  declarations: [ CreateConsentTemplateComponent, NgbdModalCreateTemplate, NgbdModalTemplate ],
  providers: [ SelectOptionService ],
  bootstrap: [ CreateConsentTemplateComponent ],
  entryComponents: [ NgbdModalCreateTemplate, NgbdModalTemplate ]
})
export class CreateConsentTemplateModule { }



