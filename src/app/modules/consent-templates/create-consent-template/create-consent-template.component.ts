import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { IOption } from 'ng-select';
import { ConsentTemplateService } from 'src/app/services/admin-function-services/consent-tmplate/consent-template.service';
import { ConsentTemplateRequest ,Purpose, Category, Frequency , FiType, DataLife, IDataOption, 
         ConsentExpiry } from 'src/app/services/admin-function-services/consent-tmplate/consent-template';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-template',
  styleUrls: ['./create-consent-template.component.scss',
              '../../../../assets/icon/icofont/css/icofont.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">{{templateName}} is already exists.</p>
      <button type="button" 
              class="btn btn-outline-primary mb-2 mr-2" 
              (click)="modal.dismiss('cancel click')"
              style="margin-top:18px">
            Ok
      </button>
  </div>`
})

export class NgbdModalTemplate {
  @Input() templateName: string;
  constructor(public modal: NgbActiveModal) {}
}

@Component({
  selector: 'ngbd-modal-create-template',
  styleUrls: ['./create-consent-template.component.scss',
              '../../../../assets/icon/icofont/css/icofont.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
  `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Your template has been created.</p>
      <button type="button" 
              class="btn btn-outline-primary mb-2 mr-2" 
              [routerLink]="['/consent-templates/pending-consent-template/']"
              (click)="modal.dismiss('cancel click')"
              style="margin-top:18px">
            Ok
      </button>
  </div>`
})

export class NgbdModalCreateTemplate {
  constructor(public modal: NgbActiveModal) {}
}

@Component({
  selector: 'app-create-consent-template',
  templateUrl: './create-consent-template.component.html',
  styleUrls: ['./create-consent-template.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})

export class CreateConsentTemplateComponent implements OnInit {

  public consentTemplateForm : FormGroup; consentTemplateRowData;
  public onetime = true; periodic = true; addConsentTemplate = true; isConsentTemplateExist:boolean = false;
  isSelect: true;
  public fiTypesList = []; fiTypeData: FiType[]; fiTypes = []; consentTypesList = []; purposeList = [];
  checkBoxList = [];
  public CheckBoxesList: any; radioSelValidity:any; selectFiTypes: any; selectConsentTypes: any; 
  consentTemplateList: any; consentExpiry: any; purpose: any
  public selectedValidity:string; frequencyUnit: String; purposeCode: String;
  public frequencyValue: number; selectedDataRangeStartMonths: number; defaultConsentStartValue: number;

  fetchTypes = ['ONETIME', 'PERIODIC'];
  frequencies = ['DAY', 'HOUR', 'MONTH', 'YEAR'];
  consentExpiryUnits = [ 'DAY', 'MONTH', 'YEAR' ];
  consentModes = ['QUERY', 'VIEW', 'STORE', 'STREAM'];
  dataLifeUnitList = ['DAY', 'INF', 'MONTH', 'YEAR'];
  dateRangeUnits = ['current', 'last 1', 'last 3', 'last 6', 'last 9', 'last 12', 'last 18'];

  constructor(private consentTemplateService: ConsentTemplateService,
              private formBuilder: FormBuilder, 
              private router: Router, 
              private modalService: NgbModal, 
              private simpleModalService: SimpleModalService) {

    this.consentTemplateForm = this.formBuilder.group({
      templateName: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.templateNameLength])],
      templateDescription: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator, this.templateDescriptionLength ])],
      consentMode: ['', Validators.required],
      consentPurpose: ['', Validators.required],
      dataLifeValue: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
      dataLifeUnit: ['', Validators.required],
      consentExpiryValue: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
      consentExpiryUnit: ['', Validators.required ],
      fiTypeList: ['', Validators.required ],
      checkBoxConsentType: ['', Validators.required ],
      fetchType: ['', Validators.required ],
      periodicFrequencyValue:  ['', Validators.compose([Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
      periodicFrequencyUnit: [''],
      consentStartValue: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
      dataRangeStartMonths: ['', Validators.required],
      dataRangeEndMonths: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]?([0-9]\d*)?$/)])],
    })
    this.defaultConsentStartValue = 0;
    this.fetchConsentTemplateList();
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  public templateNameLength(control: FormControl) {
    const islength = (control.value || '').trim().length > 32;
    const isValid = !islength;
    return isValid ? null : { 'templateNameLength': true };
  }

  public templateDescriptionLength(control: FormControl) {
    const islength = (control.value || '').trim().length > 128;
    const isValid = !islength;
    return isValid ? null : { 'templateDescriptionLength': true };
  }

  public frequency(control: FormControl) {
    const islength = (control.value || '').trim() == 0;
    const isValid = !islength;
    return isValid ? null : { 'frequency': true };
  }

  openCreateTemplateModal() {
    this.modalService.open(NgbdModalCreateTemplate,{ centered: true });
  }

  openTemplateModal(templateName: string) {
    const templateModalRef =  this.modalService.open(NgbdModalTemplate,{ centered: true });
    templateModalRef.componentInstance.templateName = templateName;
  }

  ngOnInit() {}

  fetchConsentTemplateList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.consentTemplateService.fetchConsentTemplateList().subscribe(data => {
        var response = data['body']
        console.log("ConsentTemplate List Response: "+ data);
        this.fiTypes = response['fiTypes']; 
        this.consentTypesList = response['consentTypes'];
        this.fiTypeDropDownList(this.fiTypes);
        var purposeList = response['purpose'];
        purposeList.map(item => {
          return {
            text: item.text,
          }
        }).forEach(item => this.purposeList.push(item.text));
        this.purposeList.sort((a,b) => a.localeCompare(b));
        this.consentTemplateList = response['consentTemplateRequest'];
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  consentTemplateRequest() {
    this.isConsentTemplateExist = this.checkConsentTemplateExistence(this.consentTemplateForm.get('templateName').value);
    console.log(this.isConsentTemplateExist);

    if(this.isConsentTemplateExist) {
      this.openTemplateModal(this.consentTemplateForm.get('templateName').value);
    } else if(this.CheckBoxesList == null || this.CheckBoxesList.length == 0) {
      this.simpleModalService.addModal(AlertComponent, {
        message: 'Please select atleast one consent type' ,
      })
    } else {
      if(sessionStorage.getItem("authorizationToken") != null) { 
        this.getFrequency(this.selectedValidity);
        this.getSelectedDataRangeStartMonths(this.consentTemplateForm.get('dataRangeStartMonths').value);
  
        const frequency: Frequency = {
          unit:  this.frequencyUnit,
          value: this.frequencyValue
        }
  
        const dataLife: DataLife = {
          unit: this.consentTemplateForm.get('dataLifeUnit').value,
          value: Number(this.consentTemplateForm.get('dataLifeValue').value),
        }
  
        const consentExpiry: ConsentExpiry = {
          unit: this.consentTemplateForm.get('consentExpiryUnit').value,
          value: Number(this.consentTemplateForm.get('consentExpiryValue').value),
        }
  
        const category: Category = {
          type: this.purpose.Category.type
        }
  
        const purpose: Purpose = {
          code: this.purpose.code,
          refUri: this.purpose.refUri,
          text: this.purpose.text, 
          Category: category
        }
  
        const consentTemplateRequest: ConsentTemplateRequest = {
          templateName: this.consentTemplateForm.get('templateName').value,
          templateDescription: this.consentTemplateForm.get('templateDescription').value,
          consentMode: this.consentTemplateForm.get('consentMode').value,
          consentTypes: this.checkBoxList,
          fiTypes: this.fiTypesList,
          Purpose: purpose,
          fetchType: this.selectedValidity,
          Frequency: frequency,
          DataLife:  dataLife,
          ConsentExpiry: consentExpiry,
          consentStartDays: Number(this.consentTemplateForm.get('consentStartValue').value),   
          dataRangeStartMonths: this.selectedDataRangeStartMonths,  
          dataRangeEndMonths:  Number(this.consentTemplateForm.get('dataRangeEndMonths').value),
          createdBy: null,
          creationDate: null,
          updatedBy: null,
          updationDate: null,
          approvedBy: null,
          approvedDate: null
        }
        console.log("Consent Template Request: "+ JSON.stringify(consentTemplateRequest));
        this.createConsentTemplateResponse(consentTemplateRequest);
      } else {
        this.router.navigate(['/login']);
        console.log("Authorization token is null");
      }
    }
  }

  getConsentTemplatePurpose() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      if(this.consentTemplateForm.get('consentPurpose').value == 'Wealth management service') {
        this.purposeCode = "101"
      } else if(this.consentTemplateForm.get('consentPurpose').value == 'Customer spending patterns, budget or other reportings') {
        this.purposeCode = "102"
      } else if(this.consentTemplateForm.get('consentPurpose').value == 'Aggregated statement') {
        this.purposeCode = "103"
      } else if(this.consentTemplateForm.get('consentPurpose').value == 'Explicit consent for monitoring of the accounts') {
        this.purposeCode = "104"
      } else if(this.consentTemplateForm.get('consentPurpose').value == 'Explicit one-time consent for the accounts') {
        this.purposeCode = "105"
      } else {
        console.log("Invalid purpose");
      }
      this.consentTemplateService.fetchPurpose(this.purposeCode).subscribe(data => {
        var response = data['body'];
        console.log("Purpose Response: "+ JSON.stringify(data));
        this.purpose = response
        if(this.purpose != null) {
            this.consentTemplateRequest();
        } else {
          console.log("Purpose is not found");
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }
  
  getFrequency(selectedValidity) {
    if(selectedValidity === 'ONETIME') {
      this.frequencyUnit = 'YEAR'
      this.frequencyValue = 0
      this.consentTemplateForm.get('periodicFrequencyUnit').reset();
      this.consentTemplateForm.get('periodicFrequencyValue').reset();
    } else {
      this.frequencyUnit = this.consentTemplateForm.get('periodicFrequencyUnit').value
      this.frequencyValue = Number(this.consentTemplateForm.get('periodicFrequencyValue').value)
    }
  }

  createConsentTemplateResponse(consentTemplateRequest) {
    this.consentTemplateService.createConsentTemplate(consentTemplateRequest).subscribe(data => {
      console.log("Consent Template Response: "+ JSON.stringify(data));
      if(data['errors']) {
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
          this.simpleModalService.addModal(AlertComponent, {
            message: result["errorMsg"]
          })
        }
      } else if(data['body']) {
        //this.addConsentTemplate = !this.addConsentTemplate;
        this.consentTemplateForm.reset();
        this.openCreateTemplateModal();
        this.fetchConsentTemplateList();
      } else {
        if (typeof data["body"] == 'undefined') {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Data not found'
          })
        }
      }     
    })
  }

  checkConsentTemplateExistence(templateName: string):boolean {
    return this.consentTemplateList.some(r => r.templateName === templateName);
  }

  onSelected(option: IOption) {
    this.fiTypesList.push(option.label);
    console.log("Selected fitypes:"+ this.fiTypesList);
  }

  onDeselected(option: IOption) {
    const index: number = this.fiTypesList.indexOf(option.label);
      if (index !== -1) {
        this.fiTypesList.splice(index, 1);
      }
      console.log("Deselected fitypes:"+ this.fiTypesList);
  }

  onCheckboxChange(option: IDataOption, event:any) {
    if(event.target.checked) {
      this.checkBoxList.push(option);
    }
    else {
      for(var i=0 ; i < this.checkBoxList.length; i++) {
        if(this.checkBoxList[i] == option){
          this.checkBoxList.splice(i,1);
        }
      }
    }
    this.CheckBoxesList = this.checkBoxList;
  }

isSelected(option){
  return this.checkBoxList.indexOf(option) >= 0;
}

getConsentValidity(){
  this.radioSelValidity = this.fetchTypes.find(Item => Item === this.selectedValidity);
    if(this.radioSelValidity === "ONETIME"){
      this.periodic = true;
    } else {
      this.periodic = false;
    }
}

  onvalidityChange(item){
    this.getConsentValidity();
  }

  fiTypeDropDownList(fiTypes) {
    this.selectFiTypes = fiTypes.map(data => {
      return {
          label:data.fiType,
          value:data.fiTypeDescription
      };
    }).sort((a,b) => a.label.localeCompare(b.label));
  }

  getSelectedDataRangeStartMonths(dataRangeStartMonths) {
    if(dataRangeStartMonths === 'current') {
      this.selectedDataRangeStartMonths = 0
    } else if(dataRangeStartMonths === 'last 1') {
      this.selectedDataRangeStartMonths = 1
    } else if(dataRangeStartMonths === 'last 3') {
      this.selectedDataRangeStartMonths = 3
    } else if(dataRangeStartMonths === 'last 6') {
      this.selectedDataRangeStartMonths = 6
    } else if(dataRangeStartMonths === 'last 9') {
      this.selectedDataRangeStartMonths = 9
    } else if(dataRangeStartMonths === 'last 12') {
      this.selectedDataRangeStartMonths = 12
    } else if(dataRangeStartMonths === 'last 18') {
      this.selectedDataRangeStartMonths = 18
    }
    return this.selectedDataRangeStartMonths;
  }

  // toggleConsentTemplate(){
  //   this.addConsentTemplate = !this.addConsentTemplate;
  // }
  
  formValidation() {
    var consentExpiry =  Number(this.consentTemplateForm.get('consentExpiryValue').value)
    var dateRangeEndMonths =  Number(this.consentTemplateForm.get('dataRangeEndMonths').value)
    var periodicFrequencyValue = Number(this.consentTemplateForm.get('periodicFrequencyValue').value)

    if(this.frequencyValueValidation(periodicFrequencyValue)){
      if(this.frequencyValidation()) {
        if(this.consentExpiryValidation(consentExpiry)) {
          if(this.dateRangeEndMonthsValidation(dateRangeEndMonths)) {
              this.getConsentTemplatePurpose();
          }
        }
      }
    }
		return false;
  }

  consentExpiryValidation(consentExpiry) {
		if (consentExpiry == 0 ) {
      this.simpleModalService.addModal(AlertComponent, {
        message: 'Consent expiry value can not be negative or 0',
      })
			return false;
		} 
		return true;
  }

  dateRangeEndMonthsValidation(dateRangeEndMonths) {
		if (dateRangeEndMonths < 0) {
      this.simpleModalService.addModal(AlertComponent, {
        message: 'DateRangeEndMonths can not be negative' ,
      })
			return false;
		} 
		return true;
  }

  frequencyValidation() {
    if(this.selectedValidity == 'PERIODIC') {
      var periodicFrequencyValue = this.consentTemplateForm.get('periodicFrequencyValue').value
      var periodicFrequencyUnit = this.consentTemplateForm.get('periodicFrequencyUnit').value
      if( periodicFrequencyValue == '' || periodicFrequencyUnit  == '') {
        this.simpleModalService.addModal(AlertComponent, {
            message: 'Please enter unit and value frequency use' ,
        })
        return false;
      } else{
        return true;
      }
    } 
    return true;
  }

  frequencyValueValidation(frequencyValue) {
    if(this.selectedValidity == 'PERIODIC') {
      if (frequencyValue == 0 ) {
        this.simpleModalService.addModal(AlertComponent, {
          message: 'Frequency value can not be negative or 0',
        })
        return false;
      } else{
        return true;
      }
    } 
    return true;
  }

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
      else { control.markAsTouched(); };
    });
  };

  submitForm() {
    this.markFormTouched(this.consentTemplateForm);
    if (this.consentTemplateForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.consentTemplateForm.getRawValue;
      this.formValidation();
    } else {
      console.log("Validation Failed")
    }
  };
}