import { CreateConsentTemplateModule } from './create-consent-template.module';

describe('CreateConsentTemplateModule', () => {
  let createConsentTemplateModule: CreateConsentTemplateModule;

  beforeEach(() => {
    createConsentTemplateModule = new CreateConsentTemplateModule();
  });

  it('should create an instance', () => {
    expect(createConsentTemplateModule).toBeTruthy();
  });
});
