import { PendingConsentTemplateModule } from './pending-consent-template.module';

describe('PendingConsentTemplateModule', () => {
  let pendingConsentTemplateModule: PendingConsentTemplateModule;

  beforeEach(() => {
    pendingConsentTemplateModule = new PendingConsentTemplateModule();
  });

  it('should create an instance', () => {
    expect(pendingConsentTemplateModule).toBeTruthy();
  });
});
