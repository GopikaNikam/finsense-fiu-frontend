import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingConsentTemplateComponent } from './pending-consent-template.component'

const routes: Routes = [
  {
    path: '',
    component: PendingConsentTemplateComponent,
    data: {
      title: 'Pending Consent Templates',
      icon: 'ti-anchor',
      caption: 'Pending Consent Templates.',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PendingConsentTemplateRoutingModule { }
