import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ConsentTemplateService } from 'src/app/services/admin-function-services/consent-tmplate/consent-template.service';
import { ConsentTemplateRequest, ApproveConsentTemplate } from 'src/app/services/admin-function-services/consent-tmplate/consent-template';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleModalService } from 'ngx-simple-modal';
import { AlertComponent } from 'src/app/modules/alert/alert.component';

@Component({
  selector: 'ngbd-modal-approve',
  styleUrls: ['./pending-consent-template.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
    `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to approve consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="approveTemplate(data)">
                    Approve
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalApprove {
  @Input() data

  constructor(public modal: NgbActiveModal, 
              public router: Router,
              public simpleModalService: SimpleModalService, 
              public consentTemplateService: ConsentTemplateService) { }

  approveTemplate(data) {
    const approveConsentTemplate: ApproveConsentTemplate = {
      templateName: data['templateName']
    }

    this.consentTemplateService.approveConsentTemplate(approveConsentTemplate).subscribe(data => {
      if (data['errors']) {
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
          this.simpleModalService.addModal(AlertComponent, {
            message: result["errorMsg"]
          })
        }
      } else if (data['body']) {
        this.simpleModalService.addModal(AlertComponent, {
          message: "Consent template has been approved"
        })
      } else {
        if (typeof data["body"] == 'undefined') {
          this.simpleModalService.addModal(AlertComponent, {
            message: 'Data not found'
          })
        }
      }
      console.log("Approved Consent Template Response: ", JSON.stringify(data));
      this.modal.close();
      this.router.navigate(['/consent-templates/approve-consent-template/']);
    });
  }
}

@Component({
  selector: 'ngbd-modal-reject',
  styleUrls: ['./pending-consent-template.component.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss'],
  template:
    `<div class="modal-body">
      <p style="text-align: center;font-size: 14;font-weight:bold">Are you sure?<br/>You want to reject consent template</p>
      <div class="row" style="margin-top: 18px;align: center" align="center">
        <div class="col-6">
            <button type="button" class="btn btn-outline-cancel mb-2 mr-2" (click)="modal.dismiss('cancel click')">Cancel</button>
        </div>
        <div class="col-6">
            <button type="button" 
                    class="btn btn-outline-primary mb-2 mr-2" 
                    (click)="rejectTemplate(data)">
                    Reject
            </button>
        </div>
      </div>
  </div>`
})

export class NgbdModalReject {
  @Input() data
  constructor(public modal: NgbActiveModal, public router: Router,
    public simpleModalService: SimpleModalService, public consentTemplateService: ConsentTemplateService) { }

  rejectTemplate(data) {
    this.consentTemplateService.deleteConsentTemplate(data['templateName']).subscribe(data => {
      console.log("Deleted Consent Template Response: " + data);
    });
    this.simpleModalService.addModal(AlertComponent, {
      message: 'Your template has been rejected.'
    })
    this.modal.close();
    this.router.navigate(['/consent-templates/approve-consent-template/']);
  }
}

@Component({
  selector: 'app-pending-consent-template',
  templateUrl: './pending-consent-template.component.html',
  styleUrls: ['./pending-consent-template.component.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss']
})

export class PendingConsentTemplateComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  public consentTemplatesList = true; viewTemplateButtons = true; viewConsentTemplate = true;
  viewUpdatedConsentTemplate = true; isConsentTemplateExist: boolean = false;
  public templateDescription: string; consentMode: string; purposeText: string; purposeCode: string;
  purposeCategoryType: string; fetchType: string; frequencyUnit: string; consentExpiryUnit: string;
  dataLifeUnit: string; createdBy: string; updatedBy: string; approvalStatus: string;
  public frequencyValue: number; consentExpiryValue: number; dataLifeValue: number;
  public consentTypes: any; fiTypes: any; consentStartDays: any; dataRangeStartMonths: any;
  dataRangeEndMonths: any; consentTemplateList: any;
  public creationDate: Date; updationDate: Date;
  public consentUpdatedTemplateRowData; consentTemplateRowData;

  constructor(private consentTemplateService: ConsentTemplateService,
              private router: Router,
              private modalService: NgbModal,
              private simpleModalService: SimpleModalService,
              private modal: NgbActiveModal) {
    this.fetchConsentTemplateList();
  }

  getApproveModal(data: any) {
    if (data['createdBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Creator " + data['createdBy'] + " and approver "
          + sessionStorage.getItem('currentUser') + " can not be same while approving template"
      })
    } else if (data['updatedBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Updater " + data['updatedBy'] + " and approver "
          + sessionStorage.getItem('currentUser') + " can not be same while approving template"
      })
    } else {
      this.openApproveModal(data);
    }
  }

  openApproveModal(data: any) {
    const approveModalRef = this.modalService.open(NgbdModalApprove, { centered: true });
    approveModalRef.componentInstance.data = data;
  }

  getRejectModal(data: any) {
    if (data['createdBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Creator " + data['createdBy'] + " and approver "
          + sessionStorage.getItem('currentUser') + " can not be same while approving template"
      })
    } else if (data['updatedBy'] == sessionStorage.getItem('currentUser')) {
      this.simpleModalService.addModal(AlertComponent, {
        message: "Updater " + data['updatedBy'] + " and approver "
          + sessionStorage.getItem('currentUser') + " can not be same while approving template"
      })
    } else {
      this.openRejectModal(data);
    }
  }

  openRejectModal(data: any) {
    const rejectModalRef = this.modalService.open(NgbdModalReject, { centered: true });
    rejectModalRef.componentInstance.data = data;
  }

  ngOnInit() { }

  fetchConsentTemplateList() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.consentTemplateService.fetchConsentTemplateList().subscribe(data => {
        var response = data['body']
        console.log("ConsentTemplate List Response: " + data);
        this.consentTemplateList = response['consentTemplateRequest'];
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  viewConsentTemplateRecord(templateName) {
    this.consentTemplateService.fetchConsentTemplate(templateName).subscribe(data => {
      console.log("Consent Template Response: " + JSON.stringify(data));
      var response = data['body'];
      this.viewConsentTemplateDetails(this.setValueToObject(response['consentTemplateRequest']));
      this.consentTemplatesList = false;
      this.viewUpdatedConsentTemplate = false;
      this.fetchApprovedConsentTemplate(templateName);
    });
  }

  fetchApprovedConsentTemplate(templateName) {
    this.consentTemplateService.fetchApprovedConsentTemplate(templateName).subscribe(data => {
      console.log("Approved Consent Template Response: " + JSON.stringify(data));
      if (typeof JSON.stringify(data) == 'undefined') {
        console.log("Data not found")
      } else if (data['errors']) {
        console.log("Error: ", JSON.stringify(data['errors']));
        var data1 = data['errors'];
        for (let key in data1) {
          var result = data1[key];
          console.log("data error : " + JSON.stringify(result["errorMsg"]));
        }
      } else {
        console.log("Approved Consent Template Response: " + JSON.stringify(data['body']));
        var response = data['body'];
        console.log("response: " + JSON.stringify(response));
        this.viewConsentTemplateDetail(this.setValueToObject(response['consentTemplateRequest']));
        if (this.consentTemplateRowData != null) {
          if (this.consentTemplateRowData.templateName == this.consentUpdatedTemplateRowData.templateName) {
            this.consentTemplatesList = false;
            this.viewUpdatedConsentTemplate = true;
            this.viewConsentTemplate = false;
            this.setValues(this.consentTemplateRowData, this.consentUpdatedTemplateRowData);
          }
        }
      }
    });
  }

  setValues(consentTemplateRowData, consentUpdatedTemplateRowData) {
    if (consentUpdatedTemplateRowData.templateDescription != consentTemplateRowData.templateDescription) {
      this.templateDescription = consentUpdatedTemplateRowData.templateDescription
    }

    if (consentUpdatedTemplateRowData.consentMode != consentTemplateRowData.consentMode) {
      this.consentMode = consentUpdatedTemplateRowData.consentMode
    }

    if (consentUpdatedTemplateRowData.Purpose.text != consentTemplateRowData.Purpose.text) {
      this.purposeText = consentUpdatedTemplateRowData.Purpose.text
    }

    if (consentUpdatedTemplateRowData.Purpose.code != consentTemplateRowData.Purpose.code) {
      this.purposeCode = consentUpdatedTemplateRowData.Purpose.code
    }

    if (consentUpdatedTemplateRowData.Purpose.Category.type != consentTemplateRowData.Purpose.Category.type) {
      this.purposeCategoryType = consentUpdatedTemplateRowData.Purpose.Category.type
    }

    if (JSON.stringify(consentUpdatedTemplateRowData.consentTypes) != JSON.stringify(consentTemplateRowData.consentTypes)) {
      this.consentTypes = consentUpdatedTemplateRowData.consentTypes.join(', ')
    }

    if (JSON.stringify(consentUpdatedTemplateRowData.fiTypes) != JSON.stringify(consentTemplateRowData.fiTypes)) {
      this.fiTypes = consentUpdatedTemplateRowData.fiTypes.join(', ')
    }

    if (consentUpdatedTemplateRowData.fetchType != consentTemplateRowData.fetchType) {
      this.fetchType = consentUpdatedTemplateRowData.fetchType
    }

    if (consentUpdatedTemplateRowData.Frequency.unit != consentTemplateRowData.Frequency.unit
      || consentUpdatedTemplateRowData.Frequency.value != consentTemplateRowData.Frequency.value) {
      this.frequencyUnit = consentUpdatedTemplateRowData.Frequency.unit
      this.frequencyValue = consentUpdatedTemplateRowData.Frequency.value
    }

    if (consentUpdatedTemplateRowData.consentStartDays != consentTemplateRowData.consentStartDays) {
      this.consentStartDays = consentUpdatedTemplateRowData.consentStartDays + " day(s)"
    }

    if (consentUpdatedTemplateRowData.ConsentExpiry.unit != consentTemplateRowData.ConsentExpiry.unit
      || consentUpdatedTemplateRowData.ConsentExpiry.value != consentTemplateRowData.ConsentExpiry.value) {
      this.consentExpiryValue = consentUpdatedTemplateRowData.ConsentExpiry.value
      this.consentExpiryUnit = consentUpdatedTemplateRowData.ConsentExpiry.unit
    }

    if (consentUpdatedTemplateRowData.DataLife.value != consentTemplateRowData.DataLife.value
      || consentUpdatedTemplateRowData.DataLife.unit != consentTemplateRowData.DataLife.unit) {
      this.dataLifeValue = consentUpdatedTemplateRowData.DataLife.value
      this.dataLifeUnit = consentUpdatedTemplateRowData.DataLife.unit
    }

    if (consentUpdatedTemplateRowData.dataRangeStartMonths != consentTemplateRowData.dataRangeStartMonths) {
      this.dataRangeStartMonths = "DataRange Start Months: " + consentUpdatedTemplateRowData.dataRangeStartMonths + " MONTH"
    }

    if (consentUpdatedTemplateRowData.dataRangeEndMonths != consentTemplateRowData.dataRangeEndMonths) {
      this.dataRangeEndMonths = "DataRange End Months: " + consentUpdatedTemplateRowData.dataRangeEndMonths + " MONTH"
    }

    if (consentUpdatedTemplateRowData.createdBy != consentTemplateRowData.createdBy) {
      this.createdBy = consentUpdatedTemplateRowData.createdBy
    }

    if (consentUpdatedTemplateRowData.creationDate != consentTemplateRowData.creationDate) {
      this.creationDate = consentUpdatedTemplateRowData.creationDate
    }

    if (consentUpdatedTemplateRowData.updatedBy != consentTemplateRowData.updatedBy) {
      this.updatedBy = consentUpdatedTemplateRowData.updatedBy
    }

    if (consentUpdatedTemplateRowData.updationDate != consentTemplateRowData.updationDate) {
      this.updationDate = consentUpdatedTemplateRowData.updationDate
    }
  }

  viewConsentTemplateDetails(consentUpdatedTemplateRowData) {
    this.consentUpdatedTemplateRowData = consentUpdatedTemplateRowData;
  }

  viewConsentTemplateDetail(consentTemplateRowData) {
    this.consentTemplateRowData = consentTemplateRowData;
  }

  setValueToObject(data) {
    const consentTemplateRequest: ConsentTemplateRequest = {
      templateName: data['templateName'],
      templateDescription: data['templateDescription'],
      consentMode: data['consentMode'],
      consentTypes: data['consentTypes'],
      fiTypes: data['fiTypes'],
      Purpose: data['Purpose'],
      fetchType: data['fetchType'],
      Frequency: data['Frequency'],
      DataLife: data['DataLife'],
      ConsentExpiry: data['ConsentExpiry'],
      consentStartDays: data['consentStartDays'],
      dataRangeStartMonths: data['dataRangeStartMonths'],
      dataRangeEndMonths: data['dataRangeEndMonths'],
      createdBy: data['createdBy'],
      creationDate: data['creationDate'],
      updatedBy: data['updatedBy'],
      updationDate: data['updationDate'],
      approvedBy: data['approvedBy'],
      approvedDate: data['approvedDate'],
    }
    return consentTemplateRequest;
  }

  toggleCloseTemplateRecord() {
    this.consentTemplatesList = true;
    this.viewUpdatedConsentTemplate = true;
    this.viewConsentTemplate = true;
  }
}