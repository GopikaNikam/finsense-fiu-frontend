import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Consent Templates',
      status: false
    },
    children: [
      {
        path: 'create-consent-template',
        loadChildren: './create-consent-template/create-consent-template.module#CreateConsentTemplateModule'
      },
      {
        path: 'approve-consent-template',
        loadChildren: './approve-consent-template/approve-consent-template.module#ApproveConsentTemplateModule'
      },
      {
        path: 'pending-consent-template',
        loadChildren: './pending-consent-template/pending-consent-template.module#PendingConsentTemplateModule'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsentTemplatesRoutingModule { }
