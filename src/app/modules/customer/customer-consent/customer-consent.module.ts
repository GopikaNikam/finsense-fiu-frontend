import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerConsentComponent, NgbdModalContent } from './customer-consent.component';
import { CustomerConsentRoutingModule } from './customer-consent-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    CustomerConsentRoutingModule,
    SharedModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [CustomerConsentComponent, NgbdModalContent],
  bootstrap: [CustomerConsentComponent],
  entryComponents: [NgbdModalContent]
})
export class CustomerConsentModule { }
