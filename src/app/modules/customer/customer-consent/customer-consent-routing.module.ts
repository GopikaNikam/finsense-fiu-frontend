import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerConsentComponent } from './customer-consent.component'
const routes: Routes = [
  {
    path: '',
    component: CustomerConsentComponent,
    data: {
      title: 'Customer Consent',
      icon: 'ti-anchor',
      caption: 'Customer Consent.',
      status: false
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerConsentRoutingModule { }
