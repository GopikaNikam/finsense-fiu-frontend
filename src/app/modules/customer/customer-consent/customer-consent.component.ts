
import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CustomerConsentService } from '../../../services/customer-consent/customer-consent.service'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { SimpleModalService } from 'ngx-simple-modal';
import { AlertComponent } from '../../alert/alert.component';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h6> FI Data Detail </h6>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <app-card *ngIf="(consentDetail.fiDataInfo | json) != '{}'">
        <div class="form-group row">
          <label class="col-sm-5 col-form-label">Consent Given on: </label>
          <label class="col-sm-7 col-form-label">{{consentDetail.consentStart | date:'dd-MM-yyyy hh:mm:ss'}}</label><br>
          <label class="col-sm-5 col-form-label">FI Data Range From: </label>
          <label class="col-sm-7 col-form-label">{{consentDetail.dateTimeRangeFrom | date:'dd-MM-yyyy hh:mm:ss'}}</label><br>
          <label class="col-sm-5 col-form-label">FI Data Range To: </label>
          <label class="col-sm-7 col-form-label">{{consentDetail.dateTimeRangeTo | date:'dd-MM-yyyy hh:mm:ss'}}</label><br>
          <label class="col-sm-5 col-form-label">Mode of Consent: </label>
          <label class="col-sm-7 col-form-label">{{consentDetail.consentMode}}</label><br>
          <label class="col-sm-5 col-form-label">Data life: </label>
          <label class="col-sm-7 col-form-label">{{consentDetail.DataLife.value}} {{consentDetail.DataLife.unit}}</label><br>
          <label class="col-sm-5 col-form-label">Data access Frequency: </label>
          <label class="col-sm-7 col-form-label">{{consentDetail.Frequency.value}} times a {{consentDetail.Frequency.unit}}</label>
          <label class="col-sm-5 col-form-label">Consent Id: </label>
          <label class="col-sm-7 col-form-label">{{consentDetail.consentId}}</label><br>
        </div>
    </app-card>
    <div style="font-size: 12px; height: 150px;overflow-y: auto;overflow-x: hidden">
      <table class="table">
          <thead>
            <tr class="border-solid">
                <th>FIP Id</th>
                <th>Account Number</th>
                <th>Account Type</th>
                <th>FI Type</th>
            </tr>
            </thead>
              <tbody *ngFor="let account of consentDetail.accounts">
                <tr class="table-active">
                  <td>{{account.fipId}}</td>
                  <td>{{account.accountNo}}</td>
                  <td>{{account.accountType}}</td>
                  <td>{{account.fiType}}</td> 
                </tr>
              </tbody>
        </table>  
    </div>
  </div>
  `
})

export class NgbdModalContent {
  @Input() consentDetail: any;
  constructor(public activeModal: NgbActiveModal) { }
}

@Component({
  selector: 'app-customer-consent',
  templateUrl: './customer-consent.component.html',
  styleUrls: ['./customer-consent.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss',
    '../../../../assets/icon/icofont/css/finsense-common.scss']
})
export class CustomerConsentComponent implements OnInit {

  public customerConsentSearchForm: FormGroup;
  public consentScreen = false;
  public customerConsentResponse: any;
  public customerId: string;
  public selectionModel: any;

  constructor(private customerConsentService: CustomerConsentService,
    private modalService: NgbModal,
    private router: Router,
    private formBuilder: FormBuilder,
    private simpleModalService: SimpleModalService) {

    this.customerConsentSearchForm = this.formBuilder.group({
      customerAAId: ['', Validators.compose([Validators.required, this.noWhitespaceValidator])]
    })
    simpleModalService = simpleModalService;
    router = router;
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  ngOnInit() { }

  openModal(consentDetail: any) {
    const modalRef = this.modalService.open(NgbdModalContent, { centered: true });
    modalRef.componentInstance.consentDetail = consentDetail;
  }

  getCutomerConsent() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      this.customerId = this.customerConsentSearchForm.get('customerAAId').value;

      if (this.customerId === '') {
        this.simpleModalService.addModal(AlertComponent, {
          message: 'Please enter customer id.'
        })
      } else if (this.customerId !== '' && (this.customerId.indexOf("@") == -1 || this.customerId.indexOf("@") == 0
        || (this.customerId.length - 1 - this.customerId.indexOf("@")) == 0)) {
        this.simpleModalService.addModal(AlertComponent, {
          message: 'Invalid Account Aggregator Id.(Eg.<yourid>@<aa handle>.)'
        })
      } else {
        this.customerConsentService.getCustomerConsents(this.customerId)
          .subscribe(data => {
            console.log(JSON.stringify(data));
            if (data['errors']) {
              console.log("Consent Response: ", JSON.stringify(data['errors']));
              var data1 = data['errors'];
              for (let key in data1) {
                var result = data1[key];
                console.log("data error : " + JSON.stringify(result["errorMsg"]));
                this.simpleModalService.addModal(AlertComponent, {
                  message: result["errorMsg"]
                })
              }
            } else {
              var response = data["body"];
              this.customerConsentResponse = response["customerConsents"];
              if (typeof this.customerConsentResponse == 'undefined') {
                this.simpleModalService.addModal(AlertComponent, {
                  message: 'Data not found.'
                })
              }
            }
          })
      }
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }
}

