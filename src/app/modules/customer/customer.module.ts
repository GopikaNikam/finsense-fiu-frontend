import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleModalModule } from 'ngx-simple-modal';
import {CustomerRoutingModule} from './customer-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CustomerRoutingModule,
    SimpleModalModule
  ],
  declarations: []
})
export class CustomerModule { }
