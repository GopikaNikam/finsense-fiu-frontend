import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { ConsentRequestService } from '../../../services/consent-request/consent-request.service';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { SimpleModalService } from 'ngx-simple-modal';
import { AlertComponent } from '../../alert/alert.component';
import { SelectOptionService } from '../../../shared/elements/select-option.service';
import { ConsentRequestTemplate } from '../../../services/consent-request/consent-request-summary';

@Component({
  selector: 'app-consent-request',
  templateUrl: './consent-request.component.html',
  styleUrls: ['./consent-request.component.scss',
              '../../../../assets/icon/icofont/css/finsense-common.scss']
})

export class ConsentRequestComponent implements OnInit {

  public createDetailsForm : FormGroup
  public messagePage = true;
  public requestPage = true;
  public templateList: any;
  public templateDetails: any;
  public consentDetails: any;
  public consentsRequest: any;

  constructor(private selectOptionService: SelectOptionService, 
              private consentRequestService: ConsentRequestService,
              private parserFormatter: NgbDateParserFormatter, 
              private formBuilder: FormBuilder, 
              private router: Router,
              private simpleModalService: SimpleModalService) { 
   
    this.createDetailsForm = formBuilder.group({
      customerId: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator ])],
      consentDescription: ['', Validators.compose( [Validators.required, this.noWhitespaceValidator ])],
      templateName: ['', Validators.required],
    })
    this.fetchTemplateList();
  }

  public noHandleIdValidator(control: FormControl) {
    if(!(control.value).includes('@finvu'))
    return { 'handleId': true };
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  ngOnInit() {}

  fetchTemplateList() {
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.consentRequestService.fetchTemplateList().subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } 
        console.log("Template list response" + data);   
        this.templateList = data['body'];
        this.templateList = this.templateList.filter(a => a.active == 'Y').sort((a,b) => a.templateName.localeCompare(b.templateName));
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  createConsentRequestTemplate() {
    
    const consentRequestTemplate: ConsentRequestTemplate = {
      custId: this.createDetailsForm.get('customerId').value,
      consentDescription: this.createDetailsForm.get('consentDescription').value,
      templateName: this.createDetailsForm.get('templateName').value
    }

    console.log("Create consent template request" + JSON.stringify(consentRequestTemplate));
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.consentRequestService.createConsentRequestTemplate(consentRequestTemplate).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            this.simpleModalService.addModal(AlertComponent, {
              message: result["errorMsg"]
            })
          }
        } 
        console.log("Create consent template response: " + JSON.stringify(data['body']));
        if(typeof data["body"] != 'undefined') {
          this.templateDetails = data['body'];
          this.consentsRequest = this.templateDetails.ConsentsRequest;
          this.consentDetails = this.consentsRequest.ConsentDetail
          this.requestPage = !this.requestPage;
          this.messagePage = !this.messagePage;
        }
      })
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }

  createAndSendConsentRequest() {
    
    const consentRequestTemplate: ConsentRequestTemplate = {
      custId: this.createDetailsForm.get('customerId').value,
      consentDescription: this.createDetailsForm.get('consentDescription').value,
      templateName: this.createDetailsForm.get('templateName').value
    }

    console.log("Consent template request" + JSON.stringify(consentRequestTemplate));
    if(sessionStorage.getItem("authorizationToken") != null) {
      this.consentRequestService.createAndSendConsentRequest(consentRequestTemplate).subscribe(data => {
        if(data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("data error : " + JSON.stringify(result["errorMsg"]));
            if(result["errorMsg"].includes("InvalidCustomerAddress")) {
              this.simpleModalService.addModal(AlertComponent, {
                message: "Invalid Customer Id"
              })
            } else if (result["errorMsg"].includes("InvalidRequest")) {
              this.simpleModalService.addModal(AlertComponent, {
                message: "Invalid Request"
              })
            } else {
              this.simpleModalService.addModal(AlertComponent, {
                message: result["errorMsg"]
              })
            }
          }
        } else {
          this.simpleModalService.addModal(AlertComponent, {
            message: "Consent request has been sent"
          })
        }  
        console.log("Consent template response: " + JSON.stringify(data['body']));
      })
      this.router.navigate(['/customer-list'])
      this.createDetailsForm.reset();
    } else {
      this.router.navigate(['/login']);
      console.log("Authorization token is null");
    }
  }
 
  toggleCloseTemplateRecord() {
    this.requestPage = !this.requestPage;
    this.messagePage = !this.messagePage;
  }

  submitForm() {
    this.markFormTouched(this.createDetailsForm);
    if (this.createDetailsForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.createDetailsForm.getRawValue;
      this.createConsentRequestTemplate();
    } else {
      console.log("Validation Failed.");
    }
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control);}
      else { control.markAsTouched(); };
    });
  };
}