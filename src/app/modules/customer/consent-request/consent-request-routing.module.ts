import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsentRequestComponent } from './consent-request.component'
const routes: Routes = [
  {
    path: '',
    component: ConsentRequestComponent,
    data: {
      title: 'Consent Request',
      icon: 'ti-anchor',
      caption: 'Consent Request.',
      status: false
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsentRequestRoutingModule { }
