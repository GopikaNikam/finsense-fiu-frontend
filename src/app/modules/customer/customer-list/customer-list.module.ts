import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CustomerListComponent } from './customer-list.component';
import {CustomerListRoutingModule} from './customer-list-routing.module';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    CustomerListRoutingModule,
    Ng2GoogleChartsModule
  ],
  declarations: [CustomerListComponent]
})
export class CustomerListModule { }
