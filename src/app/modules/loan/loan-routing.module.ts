import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Loan',
      status: false
    },
    children: [
      // {
      //   path: 'account-insight',
      //   loadChildren: './account-insight/account-insight.module#AccountInsightModule'
      // }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoanRoutingModule { }



