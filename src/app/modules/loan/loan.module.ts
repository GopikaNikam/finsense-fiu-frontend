import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoanRoutingModule} from './loan-routing.module'

@NgModule({
  imports: [
    CommonModule,
    LoanRoutingModule
  ],
  declarations: []
})
export class LoanModule { }
