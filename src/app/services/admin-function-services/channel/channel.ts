export interface Channel {
    channelId: String
    channelName: String
    profileId: String
    enabled: String
    keepAliveTimeout: number
    keepAliveUrl: String	
    profileName: String
    action: string
    active: string
    createdBy: string
    creationDate: Date
    updatedBy: string
    updationDate: string
    approvedBy: string
    approvedDate: string
}

export interface ApproveChannel {
    channelId: String
}