import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../config/config-service';
import { Observable , throwError } from 'rxjs';
import { WebHeader } from '../../web-header';
import { WebRequest } from '../../web-request';
import { WebResponse } from '../../web-response';
import { Profile, ApproveProfile } from 'src/app/services/admin-function-services/profile/profile';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  apiBaseUrl: string;

  constructor(private http: HttpClient,
             private configService: ConfigService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
    }

  createProfile(profile: Profile): Observable<Profile>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: profile
    };
    return this.http.post<Profile>(this.apiBaseUrl +'/Profile',request).pipe(retry(1),
    catchError(this.handleError));
  }

  createApprovedProfile(approveProfile: ApproveProfile): Observable<ApproveProfile>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: approveProfile
    };
    return this.http.post<ApproveProfile>(this.apiBaseUrl +'/Profile/Approved',request).pipe(retry(1),
    catchError(this.handleError));
  }

  fetchProfileList() {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/Profiles').pipe(retry(1),
      catchError(this.handleError));
  }

  fetchApprovedProfileList() {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/Profiles/Approved').pipe(retry(1),
      catchError(this.handleError));
  }

  fetchProfile(profileId) {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/Profile/' + profileId).pipe(retry(1),
      catchError(this.handleError));
  }

  fetchApprovedProfile(profileId) {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/Profile/Approved/' + profileId).pipe(retry(1),
      catchError(this.handleError));
  }

  updateProfile(profile: Profile): Observable<Profile>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: profile
    };
    return this.http.put<Profile>(this.apiBaseUrl +'/Profile',request).pipe(retry(1),
    catchError(this.handleError));
  }

  deleteProfile(profileId) {
    return this.http.delete<WebResponse>(this.apiBaseUrl + '/Profile/' + profileId).pipe(retry(1),
      catchError(this.handleError));
  }

  deleteApprovedProfile(profileId) {
    return this.http.delete<WebResponse>(this.apiBaseUrl + '/Profile/Approved/' + profileId).pipe(retry(1),
      catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if(error.status == 401){
      window.alert("JWT expired Please login again!");
      window.location.href = '/login'
    } else if(error.status == 500) {
      window.alert("Something went wrong!");
    }
    return throwError(errorMessage);
   }
}
