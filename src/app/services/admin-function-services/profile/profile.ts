export interface Profile {
    profileId: String
    profileName: String
    profileDescription: String
    profileApis: String[]
    action: string
    active: string	
    createdBy: String
    updatedBy: String
    creationDate: Date
    updationDate: Date
    approvedBy: String
    approvedDate: Date
}

export interface ApproveProfile {
    profileId: String
}

export interface IDataOption {
    value : string;
    apiUrl : string;
    disabled? : boolean;
}
