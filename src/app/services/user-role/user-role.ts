export interface UserRole {
  roleName: string
  roleDescription: string
  permissionGroupList: Array<string>
  permissionGroups: Array<PermissionGroup>
  permissions: any
  action: string
  active: string
  createdBy: string
  creationDate: Date
  updatedBy: string
  updationDate: string
  approvedBy: string
  approvedDate: string
}

export interface PermissionGroup {
  permissionGroup: string
  groupApis: Array<string>
}

export interface ApproveUserRole {
  roleName: string
}

export interface IDataOption {
  value : string
  disabled? : boolean;
}
