import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResponse } from '../web-response';
import { WebHeader } from '../web-header';
import { WebRequest } from '../web-request';
import { ConfigService } from '../config/config-service';
import { Observable , throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SimpleModalService } from 'ngx-simple-modal';
import { UserRole, ApproveUserRole } from './user-role';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})

export class UserRoleService {

  apiBaseUrl: string;

  constructor(public http: HttpClient, 
    public configService: ConfigService,
    public router: Router,
    public SimpleModalService: SimpleModalService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
  }

  createUserRole(userRole : UserRole): Observable<UserRole>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: userRole
    };
    return this.http.post<UserRole>(this.apiBaseUrl +'/UserRole', request).pipe(retry(1),
    catchError(this.handleError));
  }

  approveUserRole(approveUserRole : ApproveUserRole): Observable<UserRole>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: approveUserRole
    };
    return this.http.post<UserRole>(this.apiBaseUrl +'/UserRole/Approved', request).pipe(retry(1),
    catchError(this.handleError));
  }

  updateApprovedUserRole(userRole : UserRole): Observable<UserRole>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: userRole
    };
    return this.http.put<UserRole>(this.apiBaseUrl +'/UserRole/Approved', request).pipe(retry(1),
    catchError(this.handleError));
  }

  deleteApprovedUserRole(roleName) {
    return this.http.delete<WebResponse>(this.apiBaseUrl +'/UserRole/Approved/' +roleName).pipe(retry(1),
    catchError(this.handleError));
  }
  
  fetchUserRoleList() {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/UserRole').pipe(retry(1),
    catchError(this.handleError));
  }

  fetchApprovedUserRoleList() {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/UserRole/Approved').pipe(retry(1),
    catchError(this.handleError));
  }

  fetchUserRole(roleName) {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/UserRole/' +roleName).pipe(retry(1),
    catchError(this.handleError));
  }

  fetchApprovedUserRole(roleName) {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/UserRole/Approved/' +roleName).pipe(retry(1),
    catchError(this.handleError));
  }

  deleteUserRole(roleName) {
    return this.http.delete<WebResponse>(this.apiBaseUrl +'/UserRole/' +roleName).pipe(retry(1),
    catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if(error.status == 401){
      console.log("JWT expired!");
      window.location.href = '/login'
    } 
    return throwError(errorMessage);
  }
}
