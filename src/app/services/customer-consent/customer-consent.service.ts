
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResponse } from '../web-response';
import { ConfigService } from '../../services/config/config-service';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { SimpleModalService } from 'ngx-simple-modal';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})

export class CustomerConsentService {

  apiBaseUrl: string;

  constructor(private http: HttpClient,
            private configService: ConfigService,
            private SimpleModalService: SimpleModalService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
  }

  getCustomerConsents(customerId) {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/GetCustomerConsents?customerId=' + customerId).pipe(retry(1),
      catchError(this.handleInternalServerError));
  }

  handleInternalServerError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if (error.status == 401) {
      console.log("JWT expired!");
      window.location.href = '/login'
    } else if (error.status == 500) {
      console.log("Something went wrong!");
    }
    return throwError(errorMessage);
  }
}
