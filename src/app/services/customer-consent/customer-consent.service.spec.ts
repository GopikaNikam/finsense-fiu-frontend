import { TestBed, inject } from '@angular/core/testing';

import { CustomerConsentService } from './customer-consent.service';

describe('CustomerConsentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerConsentService]
    });
  });

  it('should be created', inject([CustomerConsentService], (service: CustomerConsentService) => {
    expect(service).toBeTruthy();
  }));
});
