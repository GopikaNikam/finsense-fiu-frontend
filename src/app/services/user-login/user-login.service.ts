import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { ConfigService } from '../config/config-service';
import { UserLogin } from './user-login';
import { Observable , throwError } from 'rxjs';
import { WebHeader } from '../web-header';
import { WebRequest } from '../web-request';
import { retry, catchError } from 'rxjs/operators';
import { WebResponse } from '../web-response';

@Injectable({
  providedIn: 'root'
})

export class UserLoginService {

  apiBaseUrl: string;

  constructor(private http: HttpClient,
    public configService: ConfigService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
  }

  userLogin(userLogin:UserLogin): Observable<UserLogin> {
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };

    const request: WebRequest = {
      header: header,
      body: userLogin
    };
    return this.http.post<UserLogin>(this.apiBaseUrl +'/User/Login', request).pipe (retry(1),
    catchError(this.handleError));
  }
  
  userPermissions() {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/permissions').pipe(retry(1),
    catchError(this.handlePermissionError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if(error.status == 401){
      window.alert("Invalid credentials");
    }
    return throwError(errorMessage);
  }

  handlePermissionError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if(error.status == 400){
      window.alert("Something went wrong!");
    }
    return throwError(errorMessage);
  }
}
