import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResponse } from '../web-response';
import { WebRequest } from '../web-request';
import { WebHeader } from '../web-header';
import { FiRequestSummary, UserRequest } from './firequest-summary';
import { Observable , throwError } from 'rxjs';
import { ConfigService } from '../../services/config/config-service';
import { retry, catchError } from 'rxjs/operators';
import { SimpleModalService } from 'ngx-simple-modal';
import { AlertComponent } from 'src/app/modules/alert/alert.component';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})

export class FiRequestService {
  
  public apiBaseUrl: string;

  constructor(private http: HttpClient,
              private configService: ConfigService,
    public simpleModalService: SimpleModalService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
    }

   createFiRequest(fiRequestSummary:FiRequestSummary): Observable<FiRequestSummary>{
      const header: WebHeader = {
        rid: localStorage.getItem("messageID"),
        ts: new Date(),
        channelId: 'finsense'
      };
      const request: WebRequest = {
        header: header,
        body: fiRequestSummary
      };
      return this.http.post<FiRequestSummary>(this.apiBaseUrl +'/FIRequest', request).pipe(retry(1),
      catchError(this.handleError));
  }

  getFiStatus(consentId,sessionId,consentHandle,custId) {
    return this.http.get<WebResponse>(this.apiBaseUrl +'/FIStatus/' +consentId+ '/' +sessionId+ '/' +consentHandle+ '/' +custId).pipe(retry(1),
      catchError(this.handleError));
  }
  
  getFiFetchData(custId,consentId,sessionId){
    return this.http.get<WebResponse>(this.apiBaseUrl +'/FIFetch/' +custId+ '/' +consentId+ '/' +sessionId).pipe(retry(1),
      catchError(this.handleError));
  }

  updateUserStatus(userRequest: UserRequest): Observable<UserRequest>{
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };
    const request: WebRequest = {
      header: header,
      body: userRequest
    };
    return this.http.put<UserRequest>(this.apiBaseUrl +'/UpdateUserStatus', request).pipe(retry(1),
    catchError(this.handleError));
  }

  updateCustomerStatus(custId, consentHandleId, status) {
    const userRequest : UserRequest = {
     custId: custId,
     consentHandleId: consentHandleId,
     status: status
    }
    console.log("updateCustomerStatus Request:- "+ JSON.stringify(userRequest))
    this.updateUserStatus(userRequest).subscribe(data => {
      console.log("updateCustomerStatus Response:- "+ JSON.stringify(data['body']))
    });
 }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if(error.status == 401){
      if(error.message.includes('Consent not active')){
        window.alert("Consent not active");
        window.location.href = '/login';
      } else {
        window.alert(error.message);
        window.location.href = '/login';
      }
    } else if(error.status == 500) {
        console.log(error.message);
        this.simpleModalService.addModal(AlertComponent, {
          message: 'Please approve consent' ,
        })
    } else if(error.status == 400) {
      console.log(error.message);
      this.simpleModalService.addModal(AlertComponent, {
        message: error.message,
      })
  }
    return throwError(errorMessage);
  }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                      