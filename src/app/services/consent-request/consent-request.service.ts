import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResponse } from '../web-response';
import { WebHeader } from '../web-header';
import { WebRequest } from '../web-request';
import 'rxjs/add/operator/map';
import { ConsentRequestTemplate } from '../consent-request/consent-request-summary';
import { ConfigService } from '../../services/config/config-service';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SimpleModalService } from 'ngx-simple-modal';

@Injectable({
  providedIn: 'root'
})

export class ConsentRequestService {

  apiBaseUrl: string;

  constructor(public http: HttpClient,
    public configService: ConfigService,
    public router: Router,
    public SimpleModalService: SimpleModalService) {
    this.apiBaseUrl = this.configService.apiBaseUrl;
  }

  createConsentRequestTemplate(consentRequestTemplate: ConsentRequestTemplate): Observable<ConsentRequestTemplate> {
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };

    const request: WebRequest = {
      header: header,
      body: consentRequestTemplate
    };

    return this.http.post<ConsentRequestTemplate>(this.apiBaseUrl + '/ConsentRequestTemplate', request).pipe(retry(1),
      catchError(this.handleError));
  }

  createAndSendConsentRequest(consentRequestTemplate: ConsentRequestTemplate): Observable<ConsentRequestTemplate> {
    const header: WebHeader = {
      rid: localStorage.getItem("messageID"),
      ts: new Date(),
      channelId: 'finsense'
    };

    const request: WebRequest = {
      header: header,
      body: consentRequestTemplate
    };

    return this.http.post<ConsentRequestTemplate>(this.apiBaseUrl + '/SubmitConsentRequest', request).pipe(retry(1),
      catchError(this.handleError));
  }

  getConsentDetails(customerId, consentRequestDate) {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/getConsentDetails?customerId=' + customerId + '&consentRequestDate=' + consentRequestDate).pipe(retry(1),
      catchError(this.handleInternalServerError));
  }

  checkConsentStatus(consentHandleId, custId) {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/ConsentStatus/' + consentHandleId + '/' + custId).pipe(retry(1),
      catchError(this.handleInternalServerError));
  }

  getConsentDetailsById(consentHandle) {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/getConsentDetailsById/' + consentHandle).pipe(retry(1),
      catchError(this.handleInternalServerError));
  }

  fetchTemplateList() {
    return this.http.get<WebResponse>(this.apiBaseUrl + '/Templates').pipe(retry(1),
      catchError(this.handleInternalServerError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if (error.status == 401) {
      console.log("JWT expired!");
      window.location.href = '/login'
    } else if (error.status == 500) {
      console.log("Customer id not found with Account Aggregator");
    }
    return throwError(errorMessage);
  }

  handleInternalServerError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if (error.status == 401) {
      console.log("JWT expired!");
      window.location.href = '/login'
    } else if (error.status == 500) {
      console.log("Something went wrong!");
    }
    return throwError(errorMessage);
  }
}
