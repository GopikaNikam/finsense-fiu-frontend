import { TestBed, inject } from '@angular/core/testing';

import { ConsentRequestService } from './consent-request.service';

describe('ConsentRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConsentRequestService]
    });
  });

  it('should be created', inject([ConsentRequestService], (service: ConsentRequestService) => {
    expect(service).toBeTruthy();
  }));
});
