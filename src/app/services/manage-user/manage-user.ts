export interface ManageUser {
  userId: string
  userName: string
  userRole: string
  password: string
  confirmPassword: string
  resetPassword: string
  action: string
  active: string
  createdBy: string
  creationDate: Date
  updatedBy: string
  updationDate: string
  approvedBy: string
  approvedDate: string
}

export interface ApproveManageUser {
  userId: string
}

