import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebResponse } from '../web-response';
import 'rxjs/add/operator/map';
import { throwError } from 'rxjs';
import { ConfigService } from '../../services/config/config-service';
import { map } from 'rxjs/operators';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountInsightService {

  apiBaseUrl: string;

  constructor(private http: HttpClient,
    public configService: ConfigService) {
      this.apiBaseUrl = this.configService.apiBaseUrl;
    }

    getAccountInsight(custId, consentId, sessionId, accountRefNo){
      var response = this.http.get<WebResponse>(this.apiBaseUrl +'/accountInsight/' +custId+ '/' +consentId+ '/' +sessionId+ '/' +accountRefNo);
      
      response.map((res: WebResponse) => res.header).subscribe(data =>{
        var header = [];
        header = Object.keys(data).map(e => data[e]);
        localStorage.setItem("messageID", header[0]);
      })
      return response.pipe(map((res: WebResponse) => res.body)).pipe(retry(1),
      catchError(this.handleError));
    }

    handleError(error) {
      let errorMessage = '';
      if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
      } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      if(error.status == 401 || error.status == 500){
        window.alert("Something went wrong!");
        window.location.href = '/login'
      }
      return throwError(errorMessage);
     }
}
