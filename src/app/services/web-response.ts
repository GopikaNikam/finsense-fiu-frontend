import { WebHeader } from "./web-header";
import { ErrorInfo } from "./error-info"

export interface WebResponse {
    header: WebHeader
    body: Object
    errors: ErrorInfo[]
}